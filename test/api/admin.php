<?php
require_once("conn.php");
$_POST = json_decode(file_get_contents('php://input'),true);
$query = mysqli_query($conn, "SELECT * FROM admin");
if(isset($_POST) && !empty($_POST)) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $query = mysqli_query($conn, "SELECT * FROM admin");
    $success = 'notOK';
    $user_status = '0';
    while($row = mysqli_fetch_array($query)) {
        if ($username == $row["admin_id"] && $password == $row["admin_pass"]){
            $success = 'OK';
            $user_status = $row["admin_status"];

            break;
        }
    }
    if($success == 'OK') {
        $_SESSION["user"] = 'admin';
        $user = $_SESSION['user'];
        ?>
        {
        "success": true,
        "message": "รหัสผ่านถูกต้อง",
        "username": "<?php echo "$username"; ?>",
        "password": "<?php echo "$password"; ?>",
        "user_status": "<?php echo "$user_status"; ?>"
        }
        <?php
    } else {
        ?>
        {
        "success": false,
        "message": "ไอดี ,รหัสผ่านผิด",
        "username": "<?php echo "$username"; ?>",
        "password": "<?php echo "$password"; ?>",
        "user_status": "<?php echo "$user_status"; ?>"
        }
        <?php
    }
} else {
    ?>
    {
    "success": false,
    "message": "ไม่กรอกไอดี ,รหัสผ่าน"
    }
    <?php
}
?>
