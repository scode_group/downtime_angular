-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2018 at 12:48 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scode_downtime`
--

-- --------------------------------------------------------

--
-- Table structure for table `delegate`
--

CREATE TABLE `delegate` (
  `delegate_id` int(11) NOT NULL COMMENT 'รหัสแทนงาน',
  `delegate_date` date NOT NULL COMMENT 'วันที่แทนงาน',
  `delegate_status` tinyint(1) NOT NULL COMMENT 'สถานะการอนุมัติแทนงาน 0 ไม่อนุมัติ, 1 อนุมัติ',
  `user_id` varchar(10) COLLATE utf8_croatian_ci NOT NULL COMMENT 'id ผู้ใช้'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(5) NOT NULL COMMENT 'id พนักงาน',
  `EnNo` int(5) DEFAULT NULL COMMENT 'รหัสลายนิ้วมือ',
  `emp_name` varchar(30) COLLATE utf8_croatian_ci NOT NULL COMMENT 'ชื่อจริง',
  `emp_lastname` varchar(30) COLLATE utf8_croatian_ci NOT NULL COMMENT 'นามสกุล',
  `emp_nickname` varchar(20) COLLATE utf8_croatian_ci NOT NULL COMMENT 'ขื่อเล่น',
  `emp_address` text COLLATE utf8_croatian_ci NOT NULL COMMENT 'ที่อยู่',
  `emp_birthday` date NOT NULL COMMENT 'วันเกิด',
  `emp_weight` float(3,2) NOT NULL COMMENT 'น้ำหนัก',
  `emp_height` float(3,2) NOT NULL COMMENT 'ส่วนสูง',
  `emp_img` varchar(50) COLLATE utf8_croatian_ci NOT NULL COMMENT 'ที่อยู่รูปภาพ',
  `user_id` varchar(10) COLLATE utf8_croatian_ci NOT NULL COMMENT 'id ผู้ใช้',
  `emp_routine` tinyint(1) NOT NULL COMMENT 'สถานะ 0.พนักงานชั่วคราว 1.พนักงานประจำ',
  `emp_still` tinyint(1) NOT NULL COMMENT 'สถานะ 0.ไม่ทำงานแล้ว 1.ยังทำงานอยู่'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fingetscan`
--

CREATE TABLE `fingetscan` (
  `scan_id` int(11) NOT NULL COMMENT 'รหัสแสกนลายนิ้วมือ',
  `EnNo` int(5) NOT NULL COMMENT 'รหัสลายนิ้วมือ จากเครื่องแสกน',
  `scan_name` varchar(20) COLLATE utf8_croatian_ci NOT NULL COMMENT 'ชื่อพนักงาน จากเครื่องแสกน',
  `scan_datetime` datetime NOT NULL COMMENT 'เวลาที่แสกน จากเครื่องแสกน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `history_id` int(5) NOT NULL COMMENT 'รหัสประวัติการใช้งาน',
  `history_text` text COLLATE utf8_croatian_ci NOT NULL COMMENT 'บันทึกประวัติการใช้งาน',
  `history_datetime` datetime NOT NULL COMMENT 'เวลาที่บันทึกข้อมูล',
  `history_status` int(1) NOT NULL COMMENT 'สถานะ',
  `user_id` varchar(10) COLLATE utf8_croatian_ci NOT NULL COMMENT 'id ผู้ใช้'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notstrike`
--

CREATE TABLE `notstrike` (
  `notStrike_id` int(11) NOT NULL COMMENT 'ไอดีห้ามหยุด',
  `notStrike_comment` text COLLATE utf8_croatian_ci NOT NULL COMMENT 'สาเหตุที่ห้ามหยุด',
  `notStrike_date` date NOT NULL COMMENT 'วันที่ห้ามหยุด',
  `notStrike_routine` tinyint(1) NOT NULL COMMENT 'ใช้กับพนักงานประจำ 0 ไม่, 1 ใช่',
  `notStrike_still` tinyint(1) NOT NULL COMMENT 'ใช้กับพนักงานชั่วคราว 0 ไม่, 1 .ใช่'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settask`
--

CREATE TABLE `settask` (
  `set_id` int(11) NOT NULL COMMENT 'ไอดี แทค',
  `set_month` int(1) NOT NULL COMMENT 'พนักงานประจำหยุดได้กี่วัน/เดือน',
  `set_task` int(1) NOT NULL COMMENT 'พนักงานชั่วคราวหยุดได้กี่วัน/แทค',
  `set_alter` int(1) NOT NULL COMMENT 'แก้ไขวันทำงานได้กี่ครั้ง',
  `set_delegate` int(1) NOT NULL COMMENT 'แทนงานได้กี่วัน '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `star`
--

CREATE TABLE `star` (
  `star_id` int(11) NOT NULL COMMENT 'รหัสดาว',
  `star_1` int(1) NOT NULL COMMENT ' รูปร่างหน้าตา 1',
  `star_2` int(1) NOT NULL COMMENT ' รูปร่างหน้าตา 2',
  `star_3` int(1) NOT NULL COMMENT 'การทำงาน 1',
  `star_4` int(1) NOT NULL COMMENT 'การทำงาน 2',
  `star_5` int(1) NOT NULL COMMENT 'การปฏิสัมพันธ์กับลูกค้า ',
  `emp_id` int(5) NOT NULL COMMENT 'รหัสพนักงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strike`
--

CREATE TABLE `strike` (
  `strike_id` int(11) NOT NULL COMMENT 'รหัสหยุดงาน',
  `strike_date` date NOT NULL COMMENT 'วันที่จะหยุดงาน',
  `strike_status` tinyint(1) NOT NULL COMMENT 'สถะการอนุมัติหยุดงาน 0 ไม่อนุมัติ, 1 อนุมัติ',
  `user_id` varchar(10) COLLATE utf8_croatian_ci NOT NULL COMMENT 'id ผู้ใช้'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL COMMENT 'รหัสแทค',
  `task_name` varchar(255) COLLATE utf8_croatian_ci NOT NULL COMMENT 'ชื่อแทค',
  `task_start` date NOT NULL COMMENT 'วันเริ่มแทค',
  `task_end` date NOT NULL COMMENT 'วันสิ้นสุดแทค',
  `task_status` tinyint(1) NOT NULL COMMENT 'สถานะการเปิดใช้งาน 0 ปิด, 1 เปิด'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` varchar(10) COLLATE utf8_croatian_ci NOT NULL COMMENT 'id ผู้ใช้งาน',
  `user_pass` varchar(20) COLLATE utf8_croatian_ci NOT NULL COMMENT 'รหัสผ่าน ผู้ใช้งาน',
  `user_status` int(1) NOT NULL COMMENT 'สถานะ 1 Employee, 2 Supervisor, 3 Manager, 4 Admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_pass`, `user_status`) VALUES
('0855988010', 'aaaa', 1),
('0888888888', '1234', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delegate`
--
ALTER TABLE `delegate`
  ADD PRIMARY KEY (`delegate_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fingetscan`
--
ALTER TABLE `fingetscan`
  ADD PRIMARY KEY (`scan_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notstrike`
--
ALTER TABLE `notstrike`
  ADD PRIMARY KEY (`notStrike_id`);

--
-- Indexes for table `settask`
--
ALTER TABLE `settask`
  ADD PRIMARY KEY (`set_id`);

--
-- Indexes for table `star`
--
ALTER TABLE `star`
  ADD PRIMARY KEY (`star_id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `strike`
--
ALTER TABLE `strike`
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delegate`
--
ALTER TABLE `delegate`
  MODIFY `delegate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแทนงาน';

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'id พนักงาน';

--
-- AUTO_INCREMENT for table `fingetscan`
--
ALTER TABLE `fingetscan`
  MODIFY `scan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแสกนลายนิ้วมือ';

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `history_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'รหัสประวัติการใช้งาน';

--
-- AUTO_INCREMENT for table `notstrike`
--
ALTER TABLE `notstrike`
  MODIFY `notStrike_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดีห้ามหยุด';

--
-- AUTO_INCREMENT for table `settask`
--
ALTER TABLE `settask`
  MODIFY `set_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดี แทค';

--
-- AUTO_INCREMENT for table `star`
--
ALTER TABLE `star`
  MODIFY `star_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสดาว';

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแทค';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `delegate`
--
ALTER TABLE `delegate`
  ADD CONSTRAINT `delegate_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `star`
--
ALTER TABLE `star`
  ADD CONSTRAINT `star_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `strike`
--
ALTER TABLE `strike`
  ADD CONSTRAINT `strike_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
