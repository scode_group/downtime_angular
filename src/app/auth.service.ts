import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpHeaders, HttpEventType, HttpErrorResponse} from '@angular/common/http';
// import { Api } from './api/api';

interface myData {
    success: any,
    message: string,
    data: any[],
    user_status: number,
    emp_id: string,
    emp_data: string,
    user: {},
    admin: any[],
    status: string,
    users: any[],
    positions: any[],
    num_employee: number,
    num_superviser: number,
    num_manager: number,
    num_admin: number,
    num_all: number,
    num_allemp: number,
    num_positions: number,
    token: string,
    username: string,
    response: any[],
    emp_img: string,
    tac: any[],
    num_tac: number,
}

interface task_start {
    st_month: number,
    st_tac: number,
    st_alter: number,
    st_delegate: number,
}

interface HttpProgressEvent {
    type: HttpEventType.DownloadProgress | HttpEventType.UploadProgress,
    loaded: number,
    total?: number,
    user?: any[]
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInStatus = false
    hostUrl = 'http://127.0.0.1:8000';
    // hostUrl = 'http://scodedev.com/laravel/public';
    testData = 'asd';
    load: boolean = false;
  constructor(private http: HttpClient,
              // private api: Api
                ) { }
    host_API() {
      const API = 'http://localhost/Scode/test/scode_downTime/';
      // const API = 'http://scodedev.com/laravel/';
      return API;
    }

    host() {
        const host = 'http://localhost:4200/';
        // const host = 'http://scodedev.com/';
        return host;
    }

    loader() {
      const imageUrl: string = '/assets/icon/loader.gif';
      return imageUrl;
    }

    setLoggedIn(value: boolean) {
        this.loggedInStatus = value
    }

    get isLoggedIn() {
        return this.loggedInStatus
    }

    UpdateRegister(inputs) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/updateRegister', {
            inputs
        }, options)
    }

    UpdateRegister_M(inputs) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/updateRegister_M', {
            inputs
        }, options)
    }

    UploadImg(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/UploadImg', formData)
    }

    loginUserApi( name, password ) {
        return this.http.post<myData>(this.hostUrl + '/api/login', {
            name,
            password
        })
    }
    login( username, password ) {
        return this.http.post<myData>(this.hostUrl + '/api/login', {
            username,
            password
        })
    }

    public getHeaders(): HttpHeaders {
        let Authorization = localStorage.getItem('token');
        let headers: HttpHeaders;
        headers = new HttpHeaders()
        // .set('Authorization', this.getAuthorization())
            .set('Authorization', 'Bearer ' + Authorization)
            .set('Content-Type', 'application/json');

        // headers.append('Access-Control-Allow-Headers', 'Content-Type');
        // // headers.append('Access-Control-Allow-Methods', 'GET');
        // headers.append('Access-Control-Allow-Origin', '*');

        return headers;
    }

    newPass (number, password, conpassword, confirm) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/newPassApi', {
            number, password, conpassword, confirm
        }, options)
    }

    add_star(emp_id, starRate, starRate2, starRate3, starRate4) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/add_star', {
            emp_id, starRate, starRate2, starRate3, starRate4
        }, options)
    }

    createAccount(username, name, user_status) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        const emp_id = 1;
        const password = '123456';
        // return this.http.post<myData>(this.hostUrl + '/api/createUser', {
        //     username, name, user_status
        // }, options)
        return this.http.post<myData>(this.hostUrl + '/api/register', {
            username, name, user_status, emp_id, password
        }, options)
    }


    alluser() {
        return this.http.post<myData>(this.hostUrl + '/api/alluser', {})
    }

    allEmp() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/allEmp', {}, options)
    }

    allEmp_table() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/allEmp_table', {}, options)
    }

    allManager() {
        return this.http.post<myData>(this.hostUrl + '/api/allManager', {})
    }

    allAdmin() {
        return this.http.post<myData>(this.hostUrl + '/api/allAdmin', {})
    }

    de_user(id, number) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/de_user', {
            id, number
        }, options)
    }

    up_Status(id) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/up_Status', {
            id
        }, options)
    }

    down_Status(id) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/down_Status', {
            id
        }, options)
    }

    sms(txt, number, confirm) {
        return this.http.post<myData>(this.hostUrl + '/api/sms', {
            txt, number , confirm
        })
    }

    confirm(number) {
        return this.http.post<myData>(this.hostUrl + '/api/confirm', {
            number
        })
    }

    confirm2(number, code) {
        return this.http.post<myData>(this.hostUrl + '/api/confirm2', {
            number, code
        })
    }

    check_empData() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/check_empData', {
        }, options)
    }

    testToken() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/testToken', {
        }, options)
    }

    search_oneuser() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/search_oneuser', {

        }, options)
    }

    search_oneuser_star(emp_id) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/search_oneuser_star', {
            emp_id
        }, options)
    }

    updateImg(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/updateImg', formData)
    }
    updateHeadshot(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/updateHeadshot', formData)
    }
    updateCompcard(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/updateCompcard', formData)
    }

    updateImg_M(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/updateImg_M', formData)
    }
    updateHeadshot_M(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/updateHeadshot_M', formData)
    }
    updateCompcard_M(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/updateCompcard_M', formData)
    }

    update_star(starRate1, starRate2, starRate3, starRate4, starRate5, comment, emp_id ) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/update_star', {
            starRate1, starRate2, starRate3, starRate4, starRate5, comment, emp_id
        }, options)
    }

    create_tac(dateStart, dateEnd, insertEmp) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/create_tac', {
            dateStart, dateEnd, insertEmp
        }, options)
    }

    all_tac() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/all_tac', {
        }, options)
    }

    tac(tac_id) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/tac', {
            tac_id
        }, options)
    }

    notstrike( date ) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/notstrike', {
            date
        }, options)
    }

    Add_notstrike( date, comment, status, routine, still ) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/Add_notstrike', {
            date, comment, status, routine, still
        }, options)
    }

    de_notstrike( date ) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/de_notstrike', {
            date
        }, options)
    }

    de_tac(tac_id) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/de_tac', {
            tac_id
        }, options)
    }

    notstrike_one(date) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/notstrike_one', {
            date
        }, options)
    }

    settask_start(st_month, st_tac, st_alter, st_delegate) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/settask_start', {
            st_month, st_tac, st_alter, st_delegate
        }, options)
    }

    task_start() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<task_start>(this.hostUrl + '/api/task_start', {
        }, options)
    }

    tac_changeStatus(tac_id, event) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<task_start>(this.hostUrl + '/api/tac_changeStatus', {
            tac_id, event
        }, options)
    }

    num_tac() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/num_tac', {
        }, options)
    }

    calendar_event(date, format) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/calendar_event', {
            date, format
        }, options)
    }

    calendar_event_one(date, format, number) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/calendar_event_one', {
            date, format, number
        }, options)
    }

    add_strike(strike_start, strike_end, position, status, phone_number) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/add_strike', {
            strike_start, strike_end, position, status, phone_number
        }, options)
    }

    change_numbers() {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/change_numbers', {
        }, options)
    }
}
