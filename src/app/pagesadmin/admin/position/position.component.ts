import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { AuthService } from '../../../auth.service';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import {LocalDataSource} from "ng2-smart-table";

@Component({
  selector: 'ngx-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss']
})
export class PositionComponent implements OnInit {

    num_positions = 0;
    Positions = [];
    datas = [];
  constructor( private Auth: AuthService ) { }

  ngOnInit() {
      // this.Auth.PositionsApi().subscribe(data => {
      //     console.log (data.positions);
      //     this.Positions = data.positions;
      // })
      this.loadData();
  }

    createposition(event) {
        event.preventDefault()
        const target = event.target
        const position = target.querySelector('#position').value
        console.log ('position', position);
        // this.Auth.AddPositionsApi(position).subscribe(data => {
        //     console.log ('data', data);
        //     this.loadData();
        // })
        // this.loadData();
    }

    settings = {
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmCreate: false,
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmSave: true,
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        columns: {
            pos_name: {
                title: 'ตำแหน่งงาน',
                type: 'string',
                editable: true,
                addable: false,
            },
        },
    };
    source: LocalDataSource = new LocalDataSource();
    loadData() {
        // this.Auth.PositionsApi().subscribe(data => {
        //     console.log (data.positions);
        //     this.source.load(data.positions);
        //     this.num_positions = data.num_positions;
        // })
    }

    onDeleteConfirm(event): void {
        if (window.confirm('Are you sure you want to delete?')) {
            const pos_id = event.data.pos_id;
            console.log ('ลบข้อมูล', pos_id);
            // this.Auth.DeletePositionsApi(pos_id).subscribe(data => {
            //     console.log ('data', data);
            //     this.loadData();
            // })
        } else {
            event.confirm.reject();
        }
    }

    onEditConfirm(event): void {

    }

    onCreateConfirm(event): void {

    }


}
