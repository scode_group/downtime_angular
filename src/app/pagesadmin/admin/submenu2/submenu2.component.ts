import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth.service';

@Component({
  selector: 'ngx-submenu2',
  styleUrls: ['./submenu2.component.scss'],
  templateUrl: './submenu2.component.html',
})
export class Submenu2Component {
    index: string;

    constructor( private route: ActivatedRoute, private Auth: AuthService ) {
        this.index = this.route.snapshot.params.index;
    }

}
