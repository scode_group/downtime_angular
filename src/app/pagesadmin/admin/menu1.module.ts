import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { Menu1RoutingModule, routedComponents } from './menu1-routing.module';
import { CreateAccountComponent } from './alluser/create-account/create-account.component';
import { PositionComponent } from './position/position.component';

@NgModule({
  imports: [
    ThemeModule,
    Menu1RoutingModule,
      Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    CreateAccountComponent,
    PositionComponent,
  ],
})
export class Menu1Module { }
