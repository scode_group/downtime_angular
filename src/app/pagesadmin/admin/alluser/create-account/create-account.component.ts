import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { AuthService } from '../../../../auth.service';

@Component({
  selector: 'ngx-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {
    createForm: FormGroup;
    levels = ['Employee', 'Supervisor', 'Manager', 'Admin'];
    Positions = [];
    isSubmitted: boolean = false;
    formLevel: boolean = false;
    formPosition: boolean = false;
    constructor( private Auth: AuthService ) {
    }

  ngOnInit() {
      this.createForm = new FormGroup({
          username: new FormControl('', [
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(10),
              Validators.pattern('[0-9 ]*')
          ]),
          nickname: new FormControl('', [
              Validators.required,
          ]),
          level: new FormControl('', [
              Validators.required,
          ]),
      })
  }

    createUser(event) {
        this.isSubmitted = true;
        const createForm: boolean = this.createForm.valid;
        console.log (createForm);
        event.preventDefault()
        const target = event.target
        const username = target.querySelector('#username').value
        const nickname = target.querySelector('#nickname').value
        const level = target.querySelector('#level').value
      console.log ('createUser', username, nickname, level);
        if (createForm) {
            this.Auth.createAccount(username, nickname, level).subscribe(search => {

            })
        }
    }

}
