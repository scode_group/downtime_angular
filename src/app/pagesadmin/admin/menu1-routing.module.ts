import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menu1Component } from './menu1.component';
import { AlluserComponent } from './alluser/alluser.component';
import { Submenu2Component } from './submenu2/submenu2.component';
import { Submenu3Component } from './submenu3/submenu3.component';
import { Submenu4Component } from './submenu4/submenu4.component';
import { PositionComponent } from './position/position.component';

const routes: Routes = [{
  path: '',
  component: Menu1Component,
  children: [{
    path: 'alluser',
    component: AlluserComponent,
  }, {
      path: 'position',
      component: PositionComponent,
  }, {
    path: 'submenu2/:index',
    component: Submenu2Component,
  }, {
      path: 'submenu3',
      component: Submenu3Component,
  }, {
      path: 'submenu4',
      component: Submenu4Component,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class Menu1RoutingModule {

}

export const routedComponents = [
  Menu1Component,
    AlluserComponent,
    Submenu2Component,
    Submenu3Component,
    Submenu4Component
];
