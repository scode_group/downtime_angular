import { AfterViewInit, Component, OnDestroy , OnInit} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { AuthService } from '../../auth.service';
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'ngx-echarts-pie',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsPieComponent implements AfterViewInit, OnInit, OnDestroy {
  options: any = {};
  themeSubscription: any;
    data = ['พนักงานต้อนรับ', 'พนักงานเสิร์ฟ', 'พนักงานครัว', 'ผู้จัดการ'];
    users = [];
    // data = [];
    data2 = [
        { pos_id: 1, value: 335, name: this.data[0] },
        // { value: 310, name: 'พนักงานเสิร์ฟ' },
        // { value: 234, name: 'พนักงานครัว' },
        // { value: 135, name: 'ผู้จัดการ' },
        ];

  constructor(private theme: NbThemeService, private Auth: AuthService) {
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

        console.log ('------------------test------------------');
        let user = [];
        let positions = [];
        // this.Auth.PositionsApi().subscribe(data => {
        //     positions = data.positions;
        //
        //     positions.forEach(function (value) {
        //         const pos_data = { pos_id: value.pos_id, name: value.pos_name, value: 0 };
        //         user.push(pos_data);
        //     })
        //     this.data2 = user;
        //
        //
        //     this.Auth.all_register().subscribe(data2 => {
        //         this.users = data2.users;
        //         const data3 = this.data2;
        //         this.users.forEach(function (value) {
        //             data3.forEach(function (value2) {
        //                 if ( value.emp_position === value2.pos_id ) {
        //                     value2.value++;
        //                 }
        //             })
        //         });
        //         this.data2 = data3;
        //         // console.log ('data2', this.data2)
        //
        //         // ---------------------------------------------- //
        //
        //         const colors = config.variables;
        //         const echarts: any = config.variables.echarts;
        //         this.options = {
        //             backgroundColor: echarts.bg,
        //             color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
        //             tooltip: {
        //                 trigger: 'item',
        //                 formatter: '{a} <br/>{b} : {c}คน ({d}%)',
        //             },
        //             legend: {
        //                 orient: 'vertical',
        //                 left: 'left',
        //                 data: this.data,
        //                 textStyle: {
        //                     color: echarts.textColor,
        //                 },
        //             },
        //             series: [
        //                 {
        //                     name: 'จำนวนพนักงาน',
        //                     type: 'pie',
        //                     radius: '80%',
        //                     center: ['50%', '50%'],
        //                     data: this.data2,
        //                     itemStyle: {
        //                         emphasis: {
        //                             shadowBlur: 10,
        //                             shadowOffsetX: 0,
        //                             shadowColor: echarts.itemHoverShadowColor,
        //                         },
        //                     },
        //                     label: {
        //                         normal: {
        //                             textStyle: {
        //                                 color: echarts.textColor,
        //                             },
        //                         },
        //                     },
        //                     labelLine: {
        //                         normal: {
        //                             lineStyle: {
        //                                 color: echarts.axisLineColor,
        //                             },
        //                         },
        //                     },
        //                 },
        //             ],
        //         };
        //     });
        // });

        // ---------------------------------------------- //

      // const colors = config.variables;
      // const echarts: any = config.variables.echarts;
      //
      // this.options = {
      //   backgroundColor: echarts.bg,
      //   color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
      //   tooltip: {
      //     trigger: 'item',
      //     formatter: '{a} <br/>{b} : {c}คน ({d}%)',
      //   },
      //   legend: {
      //     orient: 'vertical',
      //     left: 'left',
      //     data: this.data,
      //     textStyle: {
      //       color: echarts.textColor,
      //     },
      //   },
      //   series: [
      //     {
      //       name: 'จำนวนพนักงาน',
      //       type: 'pie',
      //       radius: '80%',
      //       center: ['50%', '50%'],
      //       data: this.data2,
      //       itemStyle: {
      //         emphasis: {
      //           shadowBlur: 10,
      //           shadowOffsetX: 0,
      //           shadowColor: echarts.itemHoverShadowColor,
      //         },
      //       },
      //       label: {
      //         normal: {
      //           textStyle: {
      //             color: echarts.textColor,
      //           },
      //         },
      //       },
      //       labelLine: {
      //         normal: {
      //           lineStyle: {
      //             color: echarts.axisLineColor,
      //           },
      //         },
      //       },
      //     },
      //   ],
      // };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

    ngOnInit() {
        // console.log ('------------------test------------------');
        // let user = [];
        // let positions = [];
        // this.Auth.PositionsApi().subscribe(data => {
        //     // console.log ('pos_id, pos_name => ', data.positions);
        //     positions = data.positions;
        //     console.log ('pos_id, pos_name => ', positions);
        //
        //     // console.log('user', user);
        //     positions.forEach(function (value) {
        //         console.log ('positions => ', value.pos_id, ' && ', value.pos_name);
        //         let pos_data = { value: value.pos_id, name: value.pos_name };
        //         user.push(pos_data);
        //     })
        //     this.data2 = user;
        //     console.log (this.data2)
        // });
        // this.Auth.all_register().subscribe(data => {
        //     this.users = data.users;
        //     // console.log ('pie (user) => ', this.users);
        //     this.users.forEach(function (value) {
        //         positions.forEach(function (value2) {
        //             // console.log ('positions => ', value.emp_position, ' // ', value2.pos_id);
        //             if ( value.emp_position === value2.pos_id ) {
        //                 // console.log ('positions => ', value2.pos_name);
        //             }
        //         })
        //         // user.push(value.emp_position)
        //     });
        // });
        // // this.data = [];
        // this.data.forEach(function (value) {
        //     // console.log(value);
        // });
        // console.log ('(user) => ', user);
    }
}
