import {Component , OnInit} from '@angular/core';
import { AuthService } from '../../auth.service';
// import { NbThemeService } from '@nebular/theme';
// import { takeWhile } from 'rxjs/operators/takeWhile' ;

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
    allEmp = 0;
    report = 0;
    testcss = 100;

    allUser = 0;
    admin = 0;
    manager = 0;
    superviser = 0;
    employee = 0;

    constructor(private Auth: AuthService ) {
    }
    users = [];
    stars1 = [];
    stars2 = [];
    stars3 = [];
    stars4 = [];
    // hostUrl = 'http://localhost/scode_downTime/';
    hostUrl = this.Auth.host_API();
    // hostUrl = "http://scodedev.com/laravel/";
    // user_status = [];
    starRate = 2;
    starRate2 = 4;
    starRate3 = 5;
    starRate4 = 3;

    loadData() {
        for (let i = 1; i <= 5; i++) {
            if (this.starRate >= i) {
                this.stars1.push(i);
            }else {
                this.stars1.push(0);
            }
            if (this.starRate2 >= i) {
                this.stars2.push(i);
            }else {
                this.stars2.push(0);
            }
            if (this.starRate3 >= i) {
                this.stars3.push(i);
            }else {
                this.stars3.push(0);
            }
            if (this.starRate4 >= i) {
                this.stars4.push(i);
            }else {
                this.stars4.push(0);
            }
        }
    }
    ngOnInit() {
        // this.Auth.all_register().subscribe(data => {
        //     this.users = data.users;
        //     console.log (this.users);
        //     this.loadData();
        // })
        // this.Auth.num_user().subscribe(data => {
        //     this.allUser = data.num_all;
        //     this.allEmp = data.num_allemp;
        //     this.admin = data.num_admin;
        //     this.manager = data.num_manager;
        //     this.superviser = data.num_superviser;
        //     this.employee = data.num_employee;
        // })
    }


    starRating (number) {
        if (number === null) {
            number = 0;
        }
        const starRate = number;
        const stars = [];
        for (let i = 1; i <= 5; i++) {
            if (starRate >= i) {
                stars.push(i);
            }else {
                stars.push(0);
            }
        }
        return stars;
    }

}
