import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'หน้าแรก',
    icon: 'nb-home',
    link: '/admin/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
    {
        title: 'admin',
        icon: 'ion-arrow-down-b',
        children: [
            {
                title: 'สร้าง ID พนักงาน',
                link: '/admin/admin/alluser',
            },
            // {
            //     title: 'จัดการตำแหน่งงาน',
            //     link: '/admin/admin/position',
            // },
            // {
            //     title: 'จัดการตำแหน่งงาน',
            //     link: '/admin/admin/submenu2',
            // },
            // {
            //     title: 'ทดสอบ Login',
            //     link: '/admin/admin/submenu3',
            // },
            // {
            //     title: 'รายงาน',
            //     link: '/admin/admin/submenu4',
            // },
        ],
    },
    {
        title: 'logOut',
        icon: 'ion-log-out',
        link: '/logout',
        home: true,
    },
];
