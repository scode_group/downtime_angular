import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { HttpClient } from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'HI there!';
    content = `I'm cool toaster!`;
    timeout = 5000;
    toastsLimit = 8;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

    input = {};
    inputs = [];
    firstSubmitted: boolean = false;
    isSubmitted: boolean = false;
    isImages: boolean = false;
    isImages2: boolean = false;
    isImages3: boolean = false;
    profileForm: FormGroup;

    constructor(private Auth: AuthService,
                private router: Router,
                private http: HttpClient,
                private formBuilder: FormBuilder,
                private toasterService: ToasterService) { }
    ngOnInit() {
        this.profileForm = new FormGroup({
            FirstName: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(30),
                // Validators.pattern('[a-zA-Z ]*')
            ]),
            LastName: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(30)
            ]),
            NickName: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(20)
            ]),
            Birthday: new FormControl('', [
                Validators.required
            ]),
            Weight: new FormControl('', [
                Validators.required,
                Validators.min(10),
                Validators.max(300)
            ]),
            Height: new FormControl('', [
                Validators.required,
                Validators.min(20),
                Validators.max(400)
            ]),
            Address: new FormControl('', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(1000)
            ]),
        })
    }
    radioSelected: string;
    selectedFile: File = null;
    inputSex: number = 0;
    OnSubmit(event) {
        // this.showToast('default', 'ระบบ', 'กำลังกระมวลผล');
        this.isSubmitted = true;
        console.log ('isImages' , this.isImages)
        console.log ('profileForm', this.profileForm.valid);
        // console.log (this.profileForm.controls.inputFirstName.valid);
        if (this.profileForm.valid == true) {
            this.showToast('default', 'ระบบ', 'กำลังบันทึกข้อมูล');
            this.inputs = [];
            event.preventDefault()
            event.preventDefault()
            // const inputSex = this.inputSex
            const target = event.target
            const inputFirstName = target.querySelector('#inputFirstName').value
            const inputLastName = target.querySelector('#inputLastName').value
            const inputNickName = target.querySelector('#inputNickName').value
            const inputBirthday = target.querySelector('#inputBirthday').value
            const inputAddress = target.querySelector('#inputAddress').value
            const inputWeight = target.querySelector('#inputWeight').value
            const inputHeight = target.querySelector('#inputHeight').value
            this.inputs.push({
                inputSex: this.inputSex,
                inputFirstName: inputFirstName,
                inputLastName: inputLastName,
                inputNickName: inputNickName,
                inputBirthday: inputBirthday,
                inputAddress: inputAddress,
                inputWeight: inputWeight,
                inputHeight: inputHeight,
                emp_id: localStorage.getItem('number'),
                // emp_id: 1,
                emp_data: '1',
                // inputFile: this.imageUrl,
            })
            const emp_id = localStorage.getItem('number');
            // const emp_id = '1';
            const formData: FormData = new FormData();
            formData.append('Image', this.fileToUpload, this.fileToUpload.name);
            formData.append('Image2', this.fileToUpload2, this.fileToUpload2.name);
            formData.append('Image3', this.fileToUpload3, this.fileToUpload3.name);
            formData.append('emp_id', emp_id);
            this.Auth.UploadImg(formData).subscribe();

            this.Auth.UpdateRegister(this.inputs).subscribe(data => {
                console.log ('UpdateRegister ' , (<any>data).message)
                console.log ('success ' , (<any>data).success)
                if ((<any>data).success) {
                    console.log ('data  true')
                    this.showToast('success', 'ระบบ', 'บันทึกข้อมูลสำเร็จ');
                    // localStorage.setItem('emp_data', '1');
                    this.router.navigate(['/pages'])
                }else {
                    console.log ('data  false')
                }
            })
        }else {
            this.showToast('error', 'ระบบ', 'กรุณากรอกข้อมูลให้ครบถ้วน');
            if (this.imageUrl != "/assets/images/default-image.png"){
                this.isImages = true
            }else {
                this.isImages = false
            }
        }
    }

    imageUrl: string = "/assets/images/default-image.png";
    fileToUpload: File = null;
    handleFileInput(file: FileList) {
        this.fileToUpload = file.item(0);
        this.isImages = true

        //Show image preview
        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.imageUrl = event.target.result;
            console.log ("imageUrl", this.imageUrl);
        }
        reader.readAsDataURL(this.fileToUpload);
        console.log ("fileToUpload", this.fileToUpload);
    }

    imageUrl2: string = "/assets/images/default-image.png";
    fileToUpload2: File = null;
    handleFileInput2(file: FileList) {
        this.fileToUpload2 = file.item(0);
        this.isImages2 = true

        //Show image preview
        var reader2 = new FileReader();
        reader2.onload = (event: any) => {
            this.imageUrl2 = event.target.result;
            console.log ("imageUrl2", this.imageUrl2);
        }
        reader2.readAsDataURL(this.fileToUpload2);
        console.log ("fileToUpload2", this.fileToUpload2);
    }

    imageUrl3: string = "/assets/images/default-image.png";
    fileToUpload3: File = null;
    handleFileInput3(file: FileList) {
        this.fileToUpload3 = file.item(0);
        this.isImages3 = true

        //Show image preview
        var reader3 = new FileReader();
        reader3.onload = (event: any) => {
            this.imageUrl3 = event.target.result;
            console.log ("imageUrl3", this.imageUrl3);
        }
        reader3.readAsDataURL(this.fileToUpload3);
        console.log ('fileToUpload3', this.fileToUpload3);
    }

    onItemChange(item) {
        this.inputSex = item
        console.log (item);
    }


    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

}
