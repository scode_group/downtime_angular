import {Component, OnDestroy, OnInit} from '@angular/core';

import { Router } from '@angular/router';
import {AuthService} from '../../auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';


@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
    alluser = [];
    text = 'test';
    loginForm: FormGroup;
    isSubmitted: boolean = false;
    constructor(private Auth: AuthService,
                private router: Router,
                private toasterService: ToasterService) { }

    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'HI there!';
    content = `I'm cool toaster!`;
    timeout = 5000;
    toastsLimit = 8;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

    makeToast() {
        this.showToast(this.type, this.title, this.content);
    }


    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

    ngOnInit() {
        let emp_data = localStorage.getItem('emp_data');
        if (emp_data == '1') {
            this.router.navigate(['/pages']);
        }

        this.loginForm = new FormGroup({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(9),
                Validators.maxLength(15),
                Validators.pattern('[0-9 ]*')
            ]),
            password: new FormControl('', [
                Validators.required,
                // Validators.pattern('[a-zA-Z ]*')
            ])
        })

    }

    loginUser(event) {
        this.isSubmitted = true
        if (this.loginForm.valid === true) {
            this.showToast('default', 'แจ้งเตือน', 'กำลังเข้าสู่ระบบ');
            event.preventDefault()
            const target = event.target
            let username = target.querySelector('#username').value
            const password = target.querySelector('#password').value
            try {
                const sub_number1 = username.substring (0,1);
                const sub_number2 = username.substring (1);
                if ( sub_number1 === '0' ) {
                    username = '66' + sub_number2;
                    // number = '66' + '842229026';
                }
            }catch (e) {
                console.log ('error username')
            }
            this.Auth.login(username, password).subscribe(data => {
                if (data.success) {
                    localStorage.setItem('number', data.username);
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('user_status', data.user_status.toString());
                    this.showToast('success', 'เข้าสู่ระบบสำเร็จ', 'กำลังทำการเข้าสู่ระบบ');
                    // if (data.user_status === 4) {
                    //     this.router.navigate(['/admin']);
                    // }
                    switch (data.user_status) {
                        case 1 : {
                            // console.log(1);
                            // localStorage.setItem('emp_data', data.emp_data);
                            // if (success.emp_data === 0) {
                            //     this.router.navigate(['login/profile']);
                            // } else {
                            //     this.router.navigate(['/pages']);
                            // }
                            if (data.emp_id === null){
                                this.router.navigate(['login/profile']);
                            } else {
                                this.router.navigate(['pages'])
                            }
                            break;
                        }
                        case 2 : {
                            if (data.emp_id === null){
                                this.router.navigate(['login/profile']);
                            } else {
                                this.router.navigate(['pages'])
                            }
                            break;
                        }
                        case 3 : {
                            this.router.navigate(['/manager']);
                            break;
                        }
                        case 4 : {
                            this.router.navigate(['/admin']);
                            break;
                        }
                        default : {
                            // this.router.navigate(['/login']);
                            console.log('default');
                            break;
                        }
                    }
                    // if (success.change_pass) {
                    //     localStorage.setItem('username', success.user);
                    //     localStorage.setItem('token', success.token);
                    //     localStorage.setItem('user_status', '5');
                    //     switch (success.user_status) {
                    //         case 5 : {
                    //             console.log(5);
                    //             this.router.navigate(['login/change_pass']);
                    //             break;
                    //         }
                    //         default : {
                    //             this.router.navigate(['/login']);
                    //             console.log('default');
                    //             break;
                    //         }
                    //     }
                    // } else {
                    //     localStorage.setItem('token', success.token);
                    //     localStorage.setItem('emp_id', success.emp_id);
                    //     localStorage.setItem('emp_data', success.emp_data);
                    //     console.log('emp_data => ', success.emp_data);
                    //     localStorage.setItem('username', success.user);
                    //     localStorage.setItem('user_status', success.user_status);
                    //     switch (success.user_status) {
                    //         case 1 : {
                    //             console.log(1);
                    //             // localStorage.setItem('emp_data', data.emp_data);
                    //             if (success.emp_data === 0) {
                    //                 this.router.navigate(['login/profile']);
                    //             } else {
                    //                 this.router.navigate(['/pages']);
                    //             }
                    //             break;
                    //         }
                    //         case 2 : {
                    //             this.router.navigate(['/pages']);
                    //             console.log(2);
                    //             // localStorage.setItem('emp_data', data.emp_data);
                    //             if (success.emp_data === 0) {
                    //                 this.router.navigate(['login/profile']);
                    //             } else {
                    //                 this.router.navigate(['/pages']);
                    //             }
                    //             break;
                    //         }
                    //         case 3 : {
                    //             this.router.navigate(['/manager']);
                    //             console.log(3);
                    //             break;
                    //         }
                    //         case 4 : {
                    //             console.log(4);
                    //             this.router.navigate(['/admin']);
                    //             break;
                    //         }
                    //         default : {
                    //             this.router.navigate(['/login']);
                    //             console.log('default');
                    //             break;
                    //         }
                    //     }
                        this.Auth.setLoggedIn(true)
                    // }
                } else {
                    this.showToast('error', 'ไอดีหรือรหัสผ่านผิด', 'กรุณากรอกเบอร์โทรและรหัสผ่านให้ถูกต้อง');
                    this.isSubmitted = false;
                    // console.log (data.error);
                }
            })
        }else {
            this.showToast('error', 'ผิดพลาด', 'กรุณากรอกเบอร์โทรและรหัสผ่านให้ถูกต้อง');
        }
    }

}
