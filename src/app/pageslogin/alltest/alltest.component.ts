import { Component, OnInit } from '@angular/core';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'ngx-alltest',
  templateUrl: './alltest.component.html',
  styleUrls: ['./alltest.component.scss']
})
export class AlltestComponent implements OnInit {

  constructor(private toasterService: ToasterService) { }

    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'ค่าเริ่มต้น!';
    content = `ค่าเริ่มต้น!`;
    timeout = 5000;
    toastsLimit = 8;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

    makeToast() {
        this.showToast(this.type, this.title, this.content);
    }

    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }
  ngOnInit() {
      this.showToast('default', 'แจ้งเตือน', 'กำลังเข้าสู่ระบบ');
  }

}
