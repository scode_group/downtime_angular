import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-navbar',
  templateUrl: './changepage.component.html',
  styleUrls: ['./changepage.component.scss']
})
export class ChangepageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
      let user_status = localStorage.getItem('user_status');
      switch (user_status) {
          case '1' : {
              this.router.navigate(['pages'])
              break;
          }
          case '2' : {
              this.router.navigate(['pages']);
              break;
          }
          case '3' : {
              this.router.navigate(['/manager']);
              break;
          }
          case '4' : {
              this.router.navigate(['/admin']);
              break;
          }
          default : {
              this.router.navigate(['/login']);
              console.log('default');
              break;
          }
      }
  }

}
