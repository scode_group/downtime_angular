import {Component, OnInit} from '@angular/core';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';
import {AuthService} from '../../auth.service';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

    constructor(private toasterService: ToasterService,
                private Auth: AuthService) {}

    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'HI there!';
    content = `I'm cool toaster!`;
    timeout = 5000;
    toastsLimit = 5;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

    Date_set: Date;

    colors: any = {
        red: {
            primary: '#ad2121',
            secondary: '#FAE3E3'
        },
        blue: {
            primary: '#1e90ff',
            secondary: '#D1E8FF'
        },
        yellow: {
            primary: '#e3ac00',
            secondary: '#FDF1BA'
        }
    };


    makeToast() {
        this.showToast(this.type, this.title, this.content);
    }


    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

    calendar_event() {
        const date = '2018-12';
        const format = 'all';
        this.Auth.calendar_event(date, format).subscribe(data => {
            const tac = (<any>data).tac;
            const notstrike = (<any>data).notstrike;

            console.log ('tac', tac, ' && notstrike', notstrike);
        });
    }

    ngOnInit() {
        const date = '2018-12';
        const format = 'all';
        this.Auth.calendar_event(date, format).subscribe(data => {
            const tac = (<any>data).tac;
            const notstrike = (<any>data).notstrike;

            console.log ('tac', tac, ' && notstrike', notstrike);
            const events = [];
            tac.forEach(function (value) {
                console.log ('value', value);
                // console.log ('task_start =>', value.task_start);
                const viewDate_start = new Date(value.task_start);
                const viewDate_end = new Date(value.task_end);
                // console.log ('emp_nickname', value.emp_nickname);
                // console.log (value.emp_nickname, 'viewDate_start =>', viewDate_start, ' || viewDate_end =>', viewDate_end);
                const Date_set = viewDate_start;
                // console.log ('Date_set', Date_set);
                let tomorrow = viewDate_start;
                // console.log ('tomorrow =>', tomorrow);
                while (viewDate_end >= tomorrow) {
                    console.log ('tomorrow =>', tomorrow);
                    events.push({
                        title: value.emp_nickname,
                        color: {
                            primary: '#e3ac00',
                            secondary: '#FDF1BA'
                        },
                        start: tomorrow,
                        meta: {
                            type: 'warning'
                        }});
                    tomorrow = new Date(tomorrow.getTime() + (60 * 60 * 24 * 1000));
                }


                console.log ('-----------------------------------------------------------');
            });
            console.log (events);
        });

    }

}
