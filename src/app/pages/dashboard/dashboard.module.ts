import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { ToasterModule } from 'angular2-toaster';
import { DashboardComponent } from './dashboard.component';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
      ToasterModule.forRoot(),
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
