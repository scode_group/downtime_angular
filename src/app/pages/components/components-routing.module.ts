import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { Notifications2Component } from './notifications2/notifications2.component';

const routes: Routes = [{
  path: '',
  component: ComponentsComponent,
  children: [
  {
    path: 'notifications',
    component: NotificationsComponent,
  },
      {
          path: 'notifications2',
          component: Notifications2Component,
      },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComponentsRoutingModule { }

export const routedComponents = [
  ComponentsComponent,
  NotificationsComponent,
    Notifications2Component,
];
