import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EmployeeRoutingModule, routedComponents } from './employee-routing.module';
import { FullTimeComponent } from './downtime/full-time/full-time.component';
import { PartTimeComponent } from './downtime/part-time/part-time.component';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import { AllTimeComponent } from './downtime/all-time/all-time.component';
import { EditWorkingDayComponent } from './downtime/edit-working-day/edit-working-day.component';
import { UpdateWorkingComponent } from './downtime/update-working/update-working.component';
import { ToasterModule } from 'angular2-toaster';
import {ModelUpdateWorkingComponent} from './downtime/update-working/model-update-working/model-update-working.component';

@NgModule({
  imports: [
    ThemeModule,
    EmployeeRoutingModule,
      CalendarModule.forRoot({
          provide: DateAdapter,
          useFactory: adapterFactory,
      }),
      ToasterModule.forRoot(),
  ],
  declarations: [
    ...routedComponents,
    FullTimeComponent,
    PartTimeComponent,
    AllTimeComponent,
    EditWorkingDayComponent,
    UpdateWorkingComponent,
      ModelUpdateWorkingComponent,
  ],
})
export class EmployeeModule { }
