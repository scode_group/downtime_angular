import { Component, OnInit } from '@angular/core';
import {UpdateWorkingComponent} from '../update-working.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-model-update-working',
  templateUrl: './model-update-working.component.html',
  styleUrls: ['./model-update-working.component.scss']
})
export class ModelUpdateWorkingComponent implements OnInit {

  constructor(private modal: NgbModal) { }

  ngOnInit() {
  }

}
