import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ViewEncapsulation,
    OnInit, Output, EventEmitter
} from '@angular/core';
import { isSameDay, isSameMonth } from 'date-fns';
import {
    CalendarEvent,
    CalendarViewPeriod,
    CalendarMonthViewDay,
    CalendarMonthViewBeforeRenderEvent,
    CalendarWeekViewBeforeRenderEvent,
    CalendarDayViewBeforeRenderEvent
} from 'angular-calendar';
import {
    addDays,
    addHours,
    startOfDay,
    subWeeks,
    startOfMonth,
    endOfMonth,
    addWeeks, format
} from 'date-fns';
import { GetMonthViewArgs, MonthView, getMonthView } from 'calendar-utils';
import {Subject} from 'rxjs/index';
import {AuthService} from '../../../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {ModelUpdateWorkingComponent} from './model-update-working/model-update-working.component';

const RED_CELL: 'red-cell' = 'red-cell';
const BLUE_CELL: 'blue-cell' = 'blue-cell';
const YELLOW_CELL: 'yellow-cell' = 'yellow-cell';
const BLACK_CELL: 'black-cell' = 'black-cell';
const GREEN_CELL: 'green-cell' = 'green-cell';

interface Film {
    id: number;
    title: string;
    release_date: string;
}

@Component({
  selector: 'ngx-update-working',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
  templateUrl: './update-working.component.html',
  styleUrls: ['./update-working.component.scss'],
    // styles: [
    //     `
    //         .red-cell {
    //             background-color: #ad2121 !important;
    //         }
    //
    //         .blue-cell {
    //             background-color: blue !important;
    //         }
    //
    //         .yellow-cell {
    //             background-color: #e3ac00 !important;
    //         }
    //         .black-cell {
    //             background-color: #000000 !important;
    //         }
    //         .green-cell {
    //             background-color: green !important;
    //         }
    //     `
    // ]
})
export class UpdateWorkingComponent implements OnInit {

    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'HI there!';
    content = `I'm cool toaster!`;
    timeout = 6000;
    toastsLimit = 5;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;


    colors: any = {
        red: {
            primary: '#ad2121',
            secondary: '#FAE3E3'
        },
        blue: {
            primary: '#1e90ff',
            secondary: '#D1E8FF'
        },
        yellow: {
            primary: '#e3ac00',
            secondary: '#FDF1BA'
        }
    };

    positions: any[] = [{id: 1, name: 'ลางาน'},
                        {id: 2, name: 'แทนงาน'}];

    position_Any: any = {id: null, name: 'เลือกประเภท'};

    statuss: any[] = [{id: 1, name: 'ลากิจ'},
                      {id: 2, name: 'ลาป่วย'}];

    status: any = {id: null, name: 'เลือกสถานะ'};

    strike_start = null;
    strike_end = null;

    @Output() viewChange: EventEmitter<string> = new EventEmitter();

    @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

    view: string = 'month';

    viewDate: Date = new Date();

    selectDate = '2018-09-01';

    open_popup: boolean = true;

    events: CalendarEvent[] = [
        // {
        //     title: 'Event 1',
        //     color: this.colors.yellow,
        //     start: new Date('2018-12-15'),
        //     meta: {
        //         type: 'warning'
        //     }
        // },
    ];
    period: CalendarViewPeriod;

    refresh: Subject<any> = new Subject();

    cssClass: string = RED_CELL;

    clickedDate: Date;

    notstrike: any = [];

    selectedMonthViewDay: CalendarMonthViewDay;

    selectedDays: any = [];

    apiData: boolean = true;

    imageUrl: string = this.Auth.loader();

    loader: boolean = true;

    data: any = {}

    m_y: string = null;

    activeDayIsOpen: boolean = false;

    color: string = '#asdasd';

    isSubmitted: boolean = false;

    number: string = null;

    search_number: string = null;

    numberss = [
        {id: 1, number: '0855988010'},
        {id: 2, number: '0896926111'},
        {id: 3, number: '0896927512'},
        {id: 4, number: '0897856548'},
        {id: 5, number: '0897545815'},
        {id: 6, number: '0898924511'}
    ];

    constructor(private modalService: NgbModal,
                private Auth: AuthService,
                private datePipe: DatePipe,
                private route: ActivatedRoute,
                private cdr: ChangeDetectorRef,
                private toasterService: ToasterService,
                private router: Router) { }

    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

    test_model: string = 'test_model';

    beforeViewRender(
        event:
            | CalendarMonthViewBeforeRenderEvent
            | CalendarWeekViewBeforeRenderEvent
            | CalendarDayViewBeforeRenderEvent
    ) {
        this.period = event.period;
        this.cdr.detectChanges();
    }

    beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
        // กลับมาลบ
        body.forEach(cell => {
            const groups: any = {};
            cell.events.forEach((event: CalendarEvent<{ type: string }>) => {
                groups[event.meta.type] = groups[event.meta.type] || [];
                groups[event.meta.type].push(event);
            });
            cell['eventGroups'] = Object.entries(groups);
        });
        if ( this.apiData ) {
            var datepip2 = this.datePipe.transform(this.viewDate, 'yyyy-MM');
            this.Auth.notstrike( datepip2 ).subscribe( data => {
                const json = <any>data;
                this.notstrike = json.tac;
                this.apiData = false;
                this.refresh.next();
                this.loader = false;
            });
        }else {
            body.forEach(day => {
                var datepip2 = this.datePipe.transform(day.date, 'yyyy-MM-dd');
                this.notstrike.forEach(value => {
                    if (value.notStrike_date === datepip2) {
                        switch (value.notStrike_status) {
                            case 0: {
                                this.cssClass = BLUE_CELL;
                                break;
                            }
                            case 1: {
                                this.cssClass = RED_CELL;
                                break;
                            }
                            case 2: {
                                this.cssClass = BLACK_CELL;
                                break;
                            }
                        }
                        day.cssClass = this.cssClass;
                    }
                });
            });
            // console.log ('events', this.events);
            // this.apiData = true;
            // this.loader = true;
        }
    }



    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }


    showcomment(Date) {
        const datepip = this.datePipe.transform(Date, 'yyyy-MM-dd');
        // console.log ('Date', datepip, 'Tac', this.notstrike)
        let text = null;
        this.notstrike.forEach(array => {
            if (array.notStrike_date === datepip) {
                text = array.notStrike_comment;
            }
            // console.log ('array', array.notStrike_date)
        })
        return text;
    }

    loading() {
        this.loader = true;
        this.m_y = format(this.viewDate, 'MM-YYYY');
        this.apiData = true;
        this.refresh.next()
        this.refresh_carlander();
    }

    eventClicked({ event }: { event: CalendarEvent }): void {
        console.log('Event clicked', event, ' id ', event.id);
        switch (event.id) {
            case 1 : {
              console.log ('1');
              break
            }
            case 2 : {
                console.log ('2');
                break;
            }
            default : {
                console.log ('default');
                break;
            }
        }

    }

    refresh_carlander() {
        const date = this.datePipe.transform(this.viewDate, 'yyyy-MM');
        const calendar_format = 'all';
        let number = localStorage.getItem('number');
        this.Auth.calendar_event_one(date, calendar_format, number).subscribe(data => {
            const tac = (<any>data).tac;
            const notstrike = (<any>data).notstrike;
            const events = [];
            tac.forEach(function (value) {
                const viewDate_start = new Date(value.task_start);
                const viewDate_end = new Date(value.task_end);
                let tomorrow = viewDate_start;
                let color = '#e3ac00';
                let type = 'warning';
                if (value.username === number) {
                    color = 'green';
                    type = 'success';
                }
                while (viewDate_end >= tomorrow) {
                    let status = true;
                    notstrike.forEach(function (value2) {
                        const notStrike_date = new Date(value2.notStrike_date);
                        if (tomorrow.getTime() === notStrike_date.getTime()) {
                            status = false;
                        }
                    });
                    if (status) {
                        events.push({
                            id: 1,
                            title: 'วันทำงาน',
                            color: {
                                primary: color,
                                secondary: '#FDF1BA'
                            },
                            start: tomorrow,
                            meta: {
                                type: type
                            }
                        });
                    }
                    tomorrow = new Date(tomorrow.getTime() + (60 * 60 * 24 * 1000));
                }
            });
            this.events = events;
        });
    }

    updateWorking() {
        this.router.navigate(['/pages/employee/update-working']);
    }

    Onsubmit_strike () {
        this.isSubmitted = true;
        if (this.strike_start !== null &&
            this.strike_end !== null &&
            this.position_Any.id !== null &&
            this.status.id !== null) {
            console.log (true)
            let phone_number = localStorage.getItem('number')
            console.log ('strike_start', this.strike_start, 'strike_end', this.strike_end,
                'position =>', this.position_Any.id, ' status =>', this.status.id);
            this.showToast('success', 'สำเร็จ', 'กำลังประมวลผล');
            this.Auth.add_strike(this.strike_start, this.strike_end,
                this.position_Any.id, this.status.id, phone_number).subscribe(data => {
                // console.log ('data', data);
            })
        }else {
            console.log (false)
            console.log ('strike_start', this.strike_start, 'strike_end', this.strike_end,
                'position =>', this.position_Any.id, ' status =>', this.status.id);
            this.showToast('error', 'ผิดพลาด', 'กรุณากรอกข้อมูลให้ครบถ้วน');
        }
    }

    change_numbers(search_number) {
        console.log ('search_number =>', search_number)
        this.Auth.change_numbers().subscribe(date => {

        })
    }

    search_numbers() {
        console.log ('search_numbers')
    }

    select_numbers(number) {
        this.number = number;
        // this.search_number = number;
        console.log ('select_numbers =>', number)
    }

    Onsubmit_test() {
        console.log ('asdsd');
        // const activeModal = this.modalService.open(ModelUpdateWorkingComponent, { size: 'lg', container: 'nb-layout' });
        // this.modalService.open(ModelUpdateWorkingComponent);
        const activeModal = this.modalService.open(ModelUpdateWorkingComponent, { size: 'lg', container: 'nb-layout' });
        // console.log ('test', activeModal);
        // activeModal.componentInstance.Date = Date;
        // activeModal.componentInstance.Date_end = Date_end;
    }

    ngOnInit() {
        this.refresh_carlander();
    }
}