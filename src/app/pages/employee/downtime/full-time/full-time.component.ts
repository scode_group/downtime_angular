import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ViewEncapsulation,
    OnInit, Output, EventEmitter
} from '@angular/core';
import { isSameDay, isSameMonth } from 'date-fns';
import {
    CalendarEvent,
    CalendarViewPeriod,
    CalendarMonthViewDay,
    CalendarMonthViewBeforeRenderEvent,
    CalendarWeekViewBeforeRenderEvent,
    CalendarDayViewBeforeRenderEvent
} from 'angular-calendar';
import {
    addDays,
    addHours,
    startOfDay,
    subWeeks,
    startOfMonth,
    endOfMonth,
    addWeeks, format
} from 'date-fns';
import { GetMonthViewArgs, MonthView, getMonthView } from 'calendar-utils';
import {Subject} from 'rxjs/index';
import {AuthService} from '../../../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

const RED_CELL: 'red-cell' = 'red-cell';
const BLUE_CELL: 'blue-cell' = 'blue-cell';
const YELLOW_CELL: 'yellow-cell' = 'yellow-cell';
const BLACK_CELL: 'black-cell' = 'black-cell';

interface Film {
    id: number;
    title: string;
    release_date: string;
}

@Component({
  selector: 'ngx-full-time',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
  templateUrl: './full-time.component.html',
  // styleUrls: ['./full-time.component.scss']
    styles: [
        `
            .red-cell {
                background-color: #ad2121 !important;
            }

            .blue-cell {
                background-color: blue !important;
            }

            .yellow-cell {
                background-color: #e3ac00 !important;
            }
            .black-cell {
                background-color: #000000 !important;
            }
        `
    ]
})
export class FullTimeComponent implements OnInit {
    colors: any = {
        red: {
            primary: '#ad2121',
            secondary: '#FAE3E3'
        },
        blue: {
            primary: '#1e90ff',
            secondary: '#D1E8FF'
        },
        yellow: {
            primary: '#e3ac00',
            secondary: '#FDF1BA'
        }
    };

    @Output() viewChange: EventEmitter<string> = new EventEmitter();

    @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

    view: string = 'month';

    viewDate: Date = new Date();

    selectDate = '2018-09-01';

    open_popup: boolean = true;

    events: CalendarEvent[] = [
        // {
        //     title: 'Event 1',
        //     color: this.colors.yellow,
        //     start: new Date('2018-12-15'),
        //     meta: {
        //         type: 'warning'
        //     }
        // },
        // {
        //     title: 'Event 2',
        //     color: this.colors.yellow,
        //     start: new Date(),
        //     meta: {
        //         type: 'warning'
        //     }
        // },
        // {
        //     title: 'Event 3',
        //     color: this.colors.blue,
        //     start: new Date(),
        //     meta: {
        //         type: 'info'
        //     }
        // },
        // {
        //     title: 'Event 4',
        //     color: this.colors.red,
        //     start: new Date(),
        //     meta: {
        //         type: 'danger'
        //     }
        // },
        // {
        //     title: 'Event 5',
        //     color: this.colors.red,
        //     start: new Date(),
        //     meta: {
        //         type: 'danger'
        //     }
        // }
    ];
    period: CalendarViewPeriod;

    refresh: Subject<any> = new Subject();

    cssClass: string = RED_CELL;

    clickedDate: Date;

    notstrike: any = [];

    selectedMonthViewDay: CalendarMonthViewDay;

    selectedDays: any = [];

    apiData: boolean = true;

    imageUrl: string = this.Auth.loader();

    loader: boolean = true;

    data: any = {}

    m_y: string = null;

    activeDayIsOpen: boolean = false;

    color: string = '#asdasd';

  constructor(private modalService: NgbModal,
              private Auth: AuthService,
              private datePipe: DatePipe,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              private router: Router) { }

    beforeViewRender(
        event:
            | CalendarMonthViewBeforeRenderEvent
            | CalendarWeekViewBeforeRenderEvent
            | CalendarDayViewBeforeRenderEvent
    ) {
        this.period = event.period;
        this.cdr.detectChanges();
    }

    beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
        // กลับมาลบ
        body.forEach(cell => {
            const groups: any = {};
            cell.events.forEach((event: CalendarEvent<{ type: string }>) => {
                groups[event.meta.type] = groups[event.meta.type] || [];
                groups[event.meta.type].push(event);
            });
            cell['eventGroups'] = Object.entries(groups);
        });
        if ( this.apiData ) {
            // console.log ('viewDate', this.viewDate)
            var datepip2 = this.datePipe.transform(this.viewDate, 'yyyy-MM');
            // console.log ('datepip2', datepip2)
            const date_start = '2018-12-01';
            const date_end = '2018-12-30';
            this.Auth.notstrike( datepip2 ).subscribe( data => {
                const json = <any>data;
                this.notstrike = json.tac;
                // console.log('notstrike =>', this.notstrike);
                this.apiData = false;
                this.refresh.next();
                this.loader = false;
            });
        }else {
            // console.log ('apiData =>', this.apiData)
            body.forEach(day => {
                var date = day.date.toLocaleDateString();
                var datepip2 = this.datePipe.transform(day.date, 'yyyy-MM-dd');
                var splitted = date.split('/', 3);
                var dateOutput = splitted[2] + '-' + splitted[1] + '-' + splitted[0];
                var date2 = day.isToday;
                // console.log(dateOutput, '2561-12-20', this.cssClass)
                this.notstrike.forEach(value => {
                    // console.log (value.notStrike_date, datepip2)
                    if (value.notStrike_date === datepip2) {
                        switch (value.notStrike_status) {
                            case 0: {
                                this.cssClass = BLUE_CELL;
                                break;
                            }
                            case 1: {
                                this.cssClass = RED_CELL;
                                break;
                            }
                            case 2: {
                                this.cssClass = BLACK_CELL;
                                break;
                            }
                        }
                        day.cssClass = this.cssClass;
                        // console.log(' -------*--------- ')
                    }
                });
                // if (dateOutput === '2561-12-20') {
                //     day.cssClass = this.cssClass;
                //     console.log(' -------*--------- ')
                // }
                // if (day.date.getDate() % 2 === 1) {
                //     day.cssClass = this.cssClass;
                // }
                // });
            });
            // this.apiData = true;
            // this.loader = true;
        }
    }



    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        console.log ('open_popup', this.open_popup);
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }


    showcomment(Date) {
        const datepip = this.datePipe.transform(Date, 'yyyy-MM-dd');
        // console.log ('Date', datepip, 'Tac', this.notstrike)
        let text = null;
        this.notstrike.forEach(array => {
            if (array.notStrike_date === datepip) {
                text = array.notStrike_comment;
            }
            // console.log ('array', array.notStrike_date)
        })
        return text;
    }

    loading() {
        this.loader = true;
        this.m_y = format(this.viewDate, 'MM-YYYY');
        this.apiData = true;
        this.refresh.next()
        this.refresh_carlander();
    }

    refresh_carlander() {
        const date = this.datePipe.transform(this.viewDate, 'yyyy-MM');
        const format = 'all';
        this.Auth.calendar_event(date, format).subscribe(data => {
            const tac = (<any>data).tac;
            const notstrike = (<any>data).notstrike;
            const events = [];
            tac.forEach(function (value) {
                const viewDate_start = new Date(value.task_start);
                const viewDate_end = new Date(value.task_end);
                let tomorrow = viewDate_start;
                let color = '#e3ac00';
                let type = 'warning';
                let number = localStorage.getItem('number');
                if (value.username === number) {
                    color = '#0be300';
                    type = 'success';
                }
                while (viewDate_end >= tomorrow) {
                    let status = true;
                    notstrike.forEach(function (value2) {
                        const notStrike_date = new Date(value2.notStrike_date);
                        if (tomorrow.getTime() === notStrike_date.getTime()) {
                            status = false;
                        }
                    });
                    if (status) {
                        events.push({
                            title: value.emp_name + ' ' + value.emp_lastname + ' (' + value.emp_nickname + ')',
                            color: {
                                primary: color,
                                secondary: '#FDF1BA'
                            },
                            start: tomorrow,
                            meta: {
                                type: type
                            }
                        });
                    }
                    tomorrow = new Date(tomorrow.getTime() + (60 * 60 * 24 * 1000));
                }
            });
            // console.log (events);
            this.events = events;
            this.refresh.next()
        });
    }

    eventClicked({ event }: { event: CalendarEvent }): void {
        console.log('Event clicked', event);
    }

    updateWorking() {
        this.apiData = true;
        this.loader = true;
        this.router.navigate(['/pages/employee/update-working']);
    }
  ngOnInit() {
      // const date = '2018-12';

      this.refresh_carlander();
  }

}
