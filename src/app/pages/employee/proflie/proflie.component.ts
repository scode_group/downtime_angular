import {Component, OnInit} from '@angular/core';
import { AuthService } from '../../../auth.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ngx-submenu1',
  styleUrls: ['./proflie.component.scss'],
  templateUrl: './proflie.component.html',
})
export class ProflieComponent implements OnInit {

    user = {};
    edit_status = false;
    constructor(private Auth: AuthService ) {}
    hostUrl = this.Auth.host_API();
    img = this.hostUrl + 'storage/app/';
    profileForm: FormGroup;
    status: null;

    emp_headshot = null;
    emp_img = null;
    emp_compcard = null;

    ngOnInit() {
        let emp_id = localStorage.getItem('number');
        this.Auth.search_oneuser().subscribe(data => {
            this.user = (<any>data).proflie
            this.status = (<any>data).user_status
            this.emp_headshot = this.img + (<any>data).proflie.emp_headshot
            this.emp_img = this.img + (<any>data).proflie.emp_img
            this.emp_compcard = this.img + (<any>data).proflie.emp_compcard
            console.log (this.user);
        });

        this.profileForm = new FormGroup({
            FirstName: new FormControl('', [
                // Validators.required,
                Validators.minLength(1),
                Validators.maxLength(30),
                // Validators.pattern('[a-zA-Z ]*')
            ]),
            LastName: new FormControl('', [
                Validators.minLength(1),
                Validators.maxLength(30)
            ]),
            NickName: new FormControl('', [
                Validators.minLength(1),
                Validators.maxLength(20)
            ]),
            Birthday: new FormControl('', [
            ]),
            Weight: new FormControl('', [
                Validators.min(10),
                Validators.max(300)
            ]),
            Height: new FormControl('', [
                Validators.min(20),
                Validators.max(400)
            ]),
            Address: new FormControl('', [
                Validators.minLength(10),
                Validators.maxLength(1000)
            ]),
        })

    }

    edit() {
        this.edit_status = true;
    }

    save() {
        this.edit_status = false;
    }

    proflie(event) {
        // console.log (event);
        if (this.profileForm.valid == true) {
            event.preventDefault()
            const target = event.target
            const inputNickName = target.querySelector('#inputNickName').value
            const inputFirstName = target.querySelector('#inputFirstName').value
            const inputLastName = target.querySelector('#inputLastName').value
            const inputBirthday = target.querySelector('#inputBirthday').value
            const inputWeight = target.querySelector('#inputWeight').value
            const inputHeight = target.querySelector('#inputHeight').value
            const inputAddress = target.querySelector('#inputAddress').value
            const inputs = [];
            inputs.push({
                inputSex: 0,
                inputFirstName: inputFirstName,
                inputLastName: inputLastName,
                inputNickName: inputNickName,
                inputBirthday: inputBirthday,
                inputAddress: inputAddress,
                inputWeight: inputWeight,
                inputHeight: inputHeight,
                emp_id: localStorage.getItem('number'),
                // emp_id: 1,
                emp_data: '1',
                // inputFile: this.imageUrl,
            })
            console.log(inputs);
            this.Auth.UpdateRegister(inputs).subscribe(qwe => {
                const emp_id = localStorage.getItem('number');
                this.Auth.search_oneuser().subscribe(data => {
                    this.user = (<any>data).proflie
                    this.status = (<any>data).user_status
                    this.emp_headshot = this.img + (<any>data).proflie.emp_headshot
                    this.emp_img = this.img + (<any>data).proflie.emp_img
                    this.emp_compcard = this.img + (<any>data).proflie.emp_compcard
                    console.log(this.user);
                });
                this.edit_status = false;
            });
        }else {
            console.log(this.profileForm.valid);
        }
    }


    handleFileInput(file: FileList) {
        const fileToUpload = file.item(0);
        const formData: FormData = new FormData();
        const emp_id = localStorage.getItem('number');
        formData.append('Image', fileToUpload, fileToUpload.name);
        formData.append('emp_id', emp_id);
        this.Auth.updateHeadshot(formData).subscribe(qwe => {
            this.Auth.search_oneuser().subscribe(data => {
                this.user = (<any>data).proflie
                this.status = (<any>data).user_status
                this.emp_headshot = this.img + (<any>data).proflie.emp_headshot
                this.emp_img = this.img + (<any>data).proflie.emp_img
                this.emp_compcard = this.img + (<any>data).proflie.emp_compcard
                console.log (this.user);
            });
        });
    }

    handleFileInput2(file: FileList) {
        const fileToUpload = file.item(0);
        const formData: FormData = new FormData();
        const emp_id = localStorage.getItem('number');
        formData.append('Image', fileToUpload, fileToUpload.name);
        formData.append('emp_id', emp_id);
        this.Auth.updateImg(formData).subscribe(qwe => {
            this.Auth.search_oneuser().subscribe(data => {
                this.user = (<any>data).proflie
                this.status = (<any>data).user_status
                this.emp_headshot = this.img + (<any>data).proflie.emp_headshot
                this.emp_img = this.img + (<any>data).proflie.emp_img
                this.emp_compcard = this.img + (<any>data).proflie.emp_compcard
                console.log (this.user);
            });
        });
    }

    handleFileInput3(file: FileList) {
        const fileToUpload = file.item(0);
        const formData: FormData = new FormData();
        const emp_id = localStorage.getItem('number');
        formData.append('Image', fileToUpload, fileToUpload.name);
        formData.append('emp_id', emp_id);
        this.Auth.updateCompcard(formData).subscribe(qwe => {
            this.Auth.search_oneuser().subscribe(data => {
                this.user = (<any>data).proflie
                this.status = (<any>data).user_status
                this.emp_headshot = this.img + (<any>data).proflie.emp_headshot
                this.emp_img = this.img + (<any>data).proflie.emp_img
                this.emp_compcard = this.img + (<any>data).proflie.emp_compcard
                console.log (this.user);
            });
        });
    }
}
