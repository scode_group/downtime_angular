import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeComponent } from './employee.component';
import { ProflieComponent } from './proflie/proflie.component';
import { DowntimeComponent } from './downtime/downtime.component';
import { Submenu3Component } from './submenu3/submenu3.component';
import { Submenu4Component } from './submenu4/submenu4.component';
import { Submenu5Component } from './submenu5/submenu5.component';
import { UpdateWorkingComponent } from './downtime/update-working/update-working.component';
import {ModelUpdateWorkingComponent} from './downtime/update-working/model-update-working/model-update-working.component';


const routes: Routes = [{
  path: '',
  component: EmployeeComponent,
  children: [{
    path: 'proflie',
    component: ProflieComponent,
  }, {
    path: 'downtime',
    component: DowntimeComponent,
  }, {
      path: 'delegate',
      component: Submenu3Component,
  }, {
      path: 'report',
      component: Submenu4Component,
  }, {
      path: 'one',
      component: Submenu5Component,
  }, {
      path: 'update-working',
      component: UpdateWorkingComponent,
  }, {
    path: 'testModel',
        component: ModelUpdateWorkingComponent,
    }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class EmployeeRoutingModule {

}

export const routedComponents = [
  EmployeeComponent,
    ProflieComponent,
    DowntimeComponent,
    Submenu3Component,
    Submenu4Component,
    Submenu5Component,
    ModelUpdateWorkingComponent,
];
