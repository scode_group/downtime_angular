import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
// import { ProfileComponent } from './profile/profile.component';
import { ChangePassComponent } from './change-pass/change-pass.component';
import { ConfirmComponent } from './confirm/confirm.component';
// import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
  //     {
  //   path: 'user',
  //   component: DashboardComponent,
  // }, {
  //     path: 'profile',
  //     component: ProfileComponent,
  // },
      {
          path: 'confirm/:number',
          component: ConfirmComponent,
      },
      {
      path: 'change_pass/:number/:confirm',
      component: ChangePassComponent,
  }, {
    path: '',
    redirectTo: 'user',
    pathMatch: 'full',
  },
      /*{
    path: '**',
    component: NotFoundComponent,
  }*/
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
