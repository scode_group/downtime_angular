import {Component, OnDestroy, OnInit} from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import {AuthService} from '../../auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'ngx-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {


    alluser = [];
    text = 'test';
    loginForm: FormGroup;
    isSubmitted: boolean = false;
    isConpass: boolean = false;
    number: string;
    confirm: string;
    constructor(private Auth: AuthService,
                private router: Router,
                private toasterService: ToasterService,
                private route: ActivatedRoute) {
        this.number = this.route.snapshot.params.number;
        this.confirm = this.route.snapshot.params.confirm;
    }

    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'HI there!';
    content = `I'm cool toaster!`;
    timeout = 5000;
    toastsLimit = 8;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

    makeToast() {
        this.showToast(this.type, this.title, this.content);
    }


    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

    ngOnInit() {
        // this.number = localStorage.getItem('username');
        this.number = this.route.snapshot.params.number;

        this.loginForm = new FormGroup({
            password: new FormControl('', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(10)
            ]),
        })

    }

    loginUser(event) {
        this.isSubmitted = true
        if (this.loginForm.valid === true) {
            this.showToast('default', 'แจ้งเตือน', 'กำลังเข้าสู่ระบบ');
            event.preventDefault()
            const target = event.target
            const code = target.querySelector('#password').value
            const number = this.number;
            this.Auth.confirm2(number, code).subscribe(data => {
                if (data.success) {
                    this.showToast('success', 'แจ้งเตือน', 'รหัสยืนยันถูกต้อง');
                    this.router.navigate(['/register/change_pass/' + number + '/' + code]);
                }else {
                    this.showToast('error', 'แจ้งเตือน', 'รหัสยืนยันไม่ถูกต้อง');
                }
                //nFaunbriKT
            })
        }else {
            this.showToast('error', 'ผิดพลาด', 'กรุณากรอกเบอร์โทรและรหัสผ่านให้ถูกต้อง');
        }
    }

}
