import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-login',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private Auth: AuthService,
              private router: Router) { }
  ngOnInit() {
      let number = localStorage.getItem('number');
      console.log (number)
      // location.reload();
      if (number == null) {
          console.log ('logout');
          this.router.navigate(['/login']);
      }else {
          localStorage.clear();
          location.reload();
      }
  }

}
