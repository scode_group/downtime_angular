import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { D3AdvancedPieComponent } from './d3-advanced-pie.component';
import { D3PieComponent } from './d3-pie.component';
import { EchartsPieComponent } from './echarts-pie.component';
// import { EchartsPie2Component } from './echarts-pie2.component';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
      ChartModule,
      NgxChartsModule,
  ],
  declarations: [
    DashboardComponent,
      D3AdvancedPieComponent,
      D3PieComponent,
      EchartsPieComponent,
      // EchartsPie2Component,
  ],
})
export class DashboardModule { }
