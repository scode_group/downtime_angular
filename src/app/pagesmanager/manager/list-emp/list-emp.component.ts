import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth.service';


@Component({
  selector: 'ngx-list-emp',
  templateUrl: './list-emp.component.html',
  styleUrls: ['./list-emp.component.scss']
})
export class ListEmpComponent implements OnInit {

    allEmp = 0;
    report = 0;
    testcss = 100;

    allUser = 0;
    admin = 0;
    manager = 0;
    superviser = 0;
    employee = 0;

    users = [];
    stars1 = [];
    stars2 = [];
    stars3 = [];
    stars4 = [];
    hostUrl = this.Auth.host_API();
    starRate = 2;
    starRate2 = 4;
    starRate3 = 5;
    starRate4 = 3;

    constructor(private Auth: AuthService ) {
    }

    loadData() {
        for (let i = 1; i <= 5; i++) {
            if (this.starRate >= i) {
                this.stars1.push(i);
            }else {
                this.stars1.push(0);
            }
            if (this.starRate2 >= i) {
                this.stars2.push(i);
            }else {
                this.stars2.push(0);
            }
            if (this.starRate3 >= i) {
                this.stars3.push(i);
            }else {
                this.stars3.push(0);
            }
            if (this.starRate4 >= i) {
                this.stars4.push(i);
            }else {
                this.stars4.push(0);
            }
        }
    }
    ngOnInit() {

        this.Auth.allEmp().subscribe(data => {
            this.users = (<any>data).users
        });
    }


    starRating (number) {
        if (number === null) {
            number = 0;
        }
        const starRate = number;
        const stars = [];
        for (let i = 1; i <= 5; i++) {
            if (starRate >= i) {
                stars.push(i);
            }else {
                stars.push(0);
            }
        }
        return stars;
    }
}
