import {Component, OnInit} from '@angular/core';
import { AuthService } from '../../../auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import { SmartTableService } from '../../../@core/data/smart-table.service';


@Component({
  selector: 'ngx-submenu2',
  styleUrls: ['./alluser.component.scss'],
  templateUrl: './alluser.component.html',
})
export class AlluserComponent implements OnInit {
    allEmp = [];
    levels = ['Employee', 'Supervisor'];
    Positions = [];
    createForm: FormGroup;
    isSubmitted: boolean = false;
    formLevel: boolean = false;
    formPosition: boolean = false;
    data: any = {}
    ngOnInit() {
        this.createForm = new FormGroup({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(10),
                Validators.pattern('[0-9 ]*')
            ]),
            nickname: new FormControl('', [
                Validators.required,
            ]),
            level: new FormControl('', [
                Validators.required,
            ]),
        });
        this.Auth.allEmp().subscribe(data => {
            this.allEmp = (<any>data).users
        });
    }
    constructor(private service: SmartTableService,
                private Auth: AuthService) {
    }


    createUser(event) {
        this.isSubmitted = true;
        const createForm: boolean = this.createForm.valid;
        console.log (createForm);
        event.preventDefault()
        const target = event.target
        let username = target.querySelector('#username').value
        const nickname = target.querySelector('#nickname').value

        const sub_number1 = username.substring (0, 1);
        const sub_number2 = username.substring (1);
        if ( sub_number1 === '0' ) {
            username = '66' + sub_number2;
            // number = '66' + '842229026';
        }

        const level = target.querySelector('#level').value
        console.log ('createUser', username, nickname, level);
        if (createForm) {
            this.Auth.createAccount(username, nickname, level).subscribe(search => {
                this.Auth.allEmp().subscribe(data => {
                    this.allEmp = (<any>data).users
                });
            });
        }
    }
    de_data(id, number) {
        console.log ('ลบ', id);
        this.Auth.de_user(id, number).subscribe(data_de => {
            this.Auth.allEmp().subscribe(data => {
                this.allEmp = (<any>data).users
            });
        });
    }
    up_Status(id) {
        console.log ('ลบ', id);
        this.Auth.up_Status(id).subscribe(data_de => {
            this.Auth.allEmp().subscribe(data => {
                this.allEmp = (<any>data).users
            });
        });
    }
    down_Status(id) {
        console.log ('ลบ', id);
        this.Auth.down_Status(id).subscribe(data_de => {
            this.Auth.allEmp().subscribe(data => {
                this.allEmp = (<any>data).users
            });
        });
    }

    sms(id, number) {
        const sub_number1 = number.substring (0,1);
        const sub_number2 = number.substring (1);
        if ( sub_number1 === '0' ) {
            number = '66' + sub_number2;
            // number = '66' + '842229026';
        }
        console.log ('sms ', id, ' || number ', number, ' || sub_number ', sub_number1);
        const host = this.Auth.host();
        // const txt = host + '#/register/change_pass/' + number + '/' + id;
        const txt = host + '#/register/confirm/' + number;
        let confirm = null;
        this.Auth.confirm(number).subscribe(data => {
            confirm = data.response[0].confirm;
            // + '\n' + 'รหัสยืนยัน: ' + confirm
            if (window.confirm('URL: ' + txt + '                    รหัสยืนยัน: ' + confirm)) {
                this.Auth.sms(txt, number, confirm).subscribe(data2 => {
                    id
                });
            } else {
            }
        });
    }

}
