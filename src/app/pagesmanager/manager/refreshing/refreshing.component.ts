import {
    Component,
    ChangeDetectionStrategy,
    ViewEncapsulation,
    Output,
    EventEmitter,
    OnInit,
} from '@angular/core';
import { CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';
import { Subject } from 'rxjs';
import { DayViewHour } from 'calendar-utils';
import {b} from '@angular/core/src/render3';
import { DatePipe } from '@angular/common';

const RED_CELL: 'red-cell' = 'red-cell';
const BLUE_CELL: 'blue-cell' = 'blue-cell';
const BLACK_CELL: 'black-cell' = 'black-cell';
const YELLOW_CELL: 'yellow-cell' = 'yellow-cell';
const GREEN_CELL: 'green-cell' = 'green-cell';
const GRAY_CELL: 'gray-cell' = 'gray-cell';
const FRAMES_GREEN: 'frames-green' = 'frames-green';

@Component({
  selector: 'app-refreshing',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
  templateUrl: './refreshing.component.html',
    styleUrls: ['./refreshing.component.css'],
    styles: [
        `
            .red-cell {
                background-color: red !important;
                color: #ffc3be;
                text-shadow: -1px 0 #ffffff, 0 1px #ffffff,
                1px 0 #ffffff, 0 -1px #ffffff
            }

            .blue-cell {
                background-color: blue !important;
                color: #ffffff;
            }

            .black-cell {
                background-color: #000000 !important;
                color: #a1a1a1;
                text-shadow: -1px 0 #ffffff, 0 1px #ffffff,
                1px 0 #ffffff, 0 -1px #ffffff;
            }

            .yellow-cell {
                background-color: #fff6f8 !important;
                color: #3324B2;
                text-shadow: -1px 0 #ffffff, 0 1px #ffffff,
                1px 0 #ffffff, 0 -1px #ffffff;
                border-color: #23ff01;
            }

            .frames-green {
                background-color: #fff6f8 !important;
                color: #40dc7e;
                text-shadow: -1px 0 #ffffff, 0 1px #ffffff,
                1px 0 #ffffff, 0 -1px #ffffff;
                border-color: #23ff01;
            }

            .frames-green div {
                border-radius: 5px;
                border: 2px solid #40dc7e;
                font-size: 1.5em;
            }

            .green-cell {
                /*background-color: #40dc7e !important;*/
                border-radius: 5px;
                background: #40dc7e !important;
                color: #ffffff;
                /*text-shadow: -1px 0 #ffffff, 0 1px #ffffff,*/
                /*1px 0 #ffffff, 0 -1px #ffffff*/
            }

            .green-cell div {
                border: 1px solid #40dc7e;
                border-radius: 5px;
                font-size: 1.5em;
                opacity: 1;
            }

            .cal-month-view .cal-day-number {
                opacity: 1;
                font-family: 'Kanit', sans-serif;
            }

            .cal-month-view .cal-day-cell.cal-weekend .cal-day-number {
                color: rgba(46, 46, 46, 0.84);
            }

            .gray-cell {
                background-color: #f5f7f4 !important;
                color: rgba(87, 87, 87, 0.54);
                text-shadow: -1px 0 #ffffff, 0 1px #ffffff,
                1px 0 #ffffff, 0 -1px #ffffff
            }

            .cal-day-selected,
            .cal-day-selected:hover {
                background-color: deeppink !important;
            }
        `
    ]
})
export class RefreshingComponent implements OnInit{
    Months = ['Employee', 'Supervisor', 'Manager', 'Admin'];

    constructor(private datePipe: DatePipe) {}
    view = 'month';
    @Output() viewChange: EventEmitter<string> = new EventEmitter();

    @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

    selectDate = '2018-09-01';

    viewDate: Date = new Date();

    events: CalendarEvent[] = [];

    refresh: Subject<any> = new Subject();

    cssClass: string = RED_CELL;

    clickedDate: Date;

    Date: string;
    testDate: string;
    Date_day_now: string = '00';
    Date_month_now: string = 'กันยายน';
    Date_year_now: string = '2561';
    Jmonths: any = [
        {number: '01', name: 'มกราคม'},
        {number: '02', name: 'กุมภาพันธ์'},
        {number: '03', name: 'มีนาคม'},
        {number: '04', name: 'เมษายน'},
        {number: '05', name: 'พฤษภาคม'},
        {number: '06', name: 'มิถุนายน'},
        {number: '07', name: 'กรกฎาคม'},
        {number: '08', name: 'สิงหาคม'},
        {number: '09', name: 'กันยายน'},
        {number: '10', name: 'ตุลาคม'},
        {number: '11', name: 'พฤศจิกายน'},
        {number: '12', name: 'ธันวาคม'},
    ];

    SetDay: any = [
        {date: '2018-09-01', format: GREEN_CELL},
        {date: '2018-09-07', format: GREEN_CELL},
        // {date: '2018-09-27', format: BLACK_CELL},
        // {date: '2018-09-05', format: RED_CELL},
        // {date: '2018-07-10', format: RED_CELL},
        // {date: '2018-07-11', format: RED_CELL},
        // {date: '2018-09-18', format: YELLOW_CELL},
        // {date: '2018-07-21', format: YELLOW_CELL},
        // {date: '2018-09-25', format: BLACK_CELL},
        // {date: '2018-07-05', format: GREEN_CELL},
        // {date: '2018-07-03', format: GREEN_CELL},
    ]

    SetTag: any = [{
        dateStart: '2018-09-01 00:00:00',
        dateEnd: '2018-09-10 00:00:00',
        numDate: 10,
    }]

    SetTag2: any = {
        dateStart: '2018-09-01 00:00:00',
        dateEnd: '2018-09-10 00:00:00',
        numDate: 10,
    }

    selectedMonthViewDay: CalendarMonthViewDay;
    selectedDays: any = [];
    dayView: DayViewHour[];
    selectedDayViewDate: Date;
    addSetDay: boolean = false;

    refreshView(): void {
        this.cssClass = this.cssClass === RED_CELL ? BLUE_CELL : RED_CELL;
        this.refresh.next();
    }
    beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
        // console.log ('body', body);
        // console.log ('body', body);
        body.forEach(day => {
            // console.log ('day', day);
            const datepip = this.datePipe.transform(day.date, 'yyyy-MM-dd');
            this.SetTag.forEach(function (value) {
                // console.log ('numDate', value.numDate);
                const dateStart = new  Date (value.dateStart);
                const dateEnd = new  Date (value.dateEnd);
                // console.log ('dateStart', dateStart);
                // console.log ('dateEnd', dateEnd);
                // console.log ('DayDate', day.date);
                if (day.date >= dateStart && day.date <= dateEnd) {
                    day.cssClass = FRAMES_GREEN;
                    // console.log ('DayDate', day.date, ' >= dateStart', dateStart);
                } else {
                    day.cssClass = GRAY_CELL;
                }
            });


            this.SetDay.forEach(function (value) {
                // console.log (dateCal);
                if (datepip === value.date) {
                    const format = value.format;
                    day.cssClass = format;
                    // console.log ('day', day);
                    // this.selectedMonthViewDay = day;
                    // const day2 = day;
                    // this.dayNow = day2;
                    // console.log (day2);
                    // this.selectedDays.push(day);
                    // assd
                    // const selectedDateTime = day.date.getTime();
                    // const dateIndex = this.selectedDays.findIndex(
                    //     selectedDay => selectedDay.date.getTime() === selectedDateTime
                    // );
                }
            });
        });
    }
    dayClicked(day: CalendarMonthViewDay): void {
        this.clickedDate = day.date;
        // console.log ('day', day);
        this.addSetDay = true;
        this.selectedMonthViewDay = day;
        const selectedDateTime = this.selectedMonthViewDay.date.getTime();
        // console.log ('dayClicked', selectedDateTime);
        const dateIndex = this.selectedDays.findIndex(
            selectedDay => selectedDay.date.getTime() === selectedDateTime
        );
        // console.log ('selectedDays', this.selectedDays);
        const datepip = this.datePipe.transform(day.date, 'yyyy-MM-dd');
        const addDay = {date: datepip, format: GREEN_CELL};
        // console.log (addDay);
        this.addSetDay = this.compareDate(addDay.date, this.SetDay);
        // console.log (this.addSetDay);
        // if (this.addSetDay === undefined ) {
        //     this.addSetDay = true;
        // }
        // console.log (day.date.toString());
        // if ( this.addSetDay ) {
        //     this.SetDay.push(addDay);
        //     console.log (this.SetDay);
        // }
        const dateStart = new  Date (this.SetTag2.dateStart);
        const dateEnd = new  Date (this.SetTag2.dateEnd);
        // console.log (dateStart);
        // console.log (dateEnd);
        // if (day.date >= dateStart && day.date <= dateEnd) {
        //     console.log ('in');
        // } else {
        //     console.log ('out');
        // }
        console.log (this.selectedMonthViewDay);
        if (dateIndex > -1) {
            // delete this.selectedMonthViewDay.cssClass;
            this.selectedDays.splice(dateIndex, 1);
            // day.cssClass = FRAMES_GREEN;
            if (day.date >= dateStart && day.date <= dateEnd) {
                day.cssClass = FRAMES_GREEN;
                this.deleteSetDay(day.date);
            } else {
                day.cssClass = GRAY_CELL;
            }
        } else {
            this.selectedDays.push(this.selectedMonthViewDay);
            // console.log (this.selectedMonthViewDay);
            // day.cssClass = GREEN_CELL;
            this.selectedMonthViewDay = day;
            if (day.date >= dateStart && day.date <= dateEnd) {
                day.cssClass = GREEN_CELL;
                if ( this.addSetDay ) {
                    this.SetDay.push(addDay);
                    // console.log (this.SetDay);
                }
            } else {
                day.cssClass = GRAY_CELL;
            }
        }
    }

    saveTag() {
        console.log (this.SetDay);
    }

    compareDate (Day, ArrayDay) {
        let status: boolean = true;
        ArrayDay.forEach(function (value) {
            // console.log (Day, ' || ', value.date.toString())
            // console.log (addDay.date, '==', value.date)
            if (Day === value.date) {
                // this.addSetDay = true;
                status = false;
            }
        });
        return status;
    }

    deleteSetDay(date) {
        const datepip = this.datePipe.transform(date, 'yyyy-MM-dd');
        for (const j in this.SetDay) {
            // console.log(this.SetDay[j]);
            const index: number = this.SetDay[j].date.indexOf(datepip);
            // console.log (date, ' => ', index);
            if (index > -1) {
                delete this.SetDay[j];
                this.SetDay.length = this.SetDay.length - 1;
            }
        }
        // const index: number = this.SetDay[1].date.indexOf(datepip);
        // console.log (date, ' => ', index);
        // delete this.SetDay[0];
    }



    Previous() {

    }


    test($event) {
        this.testDate = $event;
        console.log ($event);
    }

    month($event) {
        // console.log ('month', $event, ' => ', this.selectDate);
        this.selectDate.substr(3, 7);
        const day = this.selectDate.substr(8, 2);
        const month = $event.number;
        const year = this.selectDate.substr(0, 4);
        const DateSt = year + '-' + month + '-' + day;
        this.selectDate = DateSt;
        this.viewDate = new Date(this.selectDate);
        console.log ('Date', this.selectDate);
        this.Date_month_now = $event.name;

    }

    year($event) {
        // console.log ('year', $event, ' => ', this.selectDate);
        this.selectDate.substr(3, 7);
        const day = this.selectDate.substr(8, 2);
        const month = this.selectDate.substr(5, 2);
        const year = $event - 543;
        const DateSt = year + '-' + month + '-' + day;
        this.selectDate = DateSt;
        this.viewDate = new Date(this.selectDate);
        console.log ('Date', this.selectDate);
        this.Date_year_now = $event;
    }

    ngOnInit() {
        // console.log ('test => ', this.viewDate);
        this.selectDate.substr(3, 7);
        const day = this.selectDate.substr(8, 2);
        const month = this.selectDate.substr(5, 2);
        const year = this.selectDate.substr(0, 4);
        const DateSt = year + '-' + month + '-' + day;
        this.Date_day_now = day;
    }

    Next() {
        console.log ('Next');
        this.viewDate = new Date('2018-01-01');
    }
}
