import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { Menu1RoutingModule, routedComponents } from './menu1-routing.module';
import { CreateTacComponent } from './create-tac/create-tac.component';


// import { ModalsComponent } from './modals/modals.component';

// import { ModalComponent } from './modals/modal/modal.component';
import { ModalTacComponent } from './create-tac/modal-tac/modal-tac.component';
import { SettingTacComponent } from './create-tac/setting-tac/setting-tac.component';

// **
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import { CalendarComponent } from './create-tac/calendar/calendar.component';
import { Calendar2Component } from './create-tac/calendar2/calendar2.component';
import { Calendar3Component } from './create-tac/calendar3/calendar3.component';
import { Calendar4Component } from './create-tac/calendar4/calendar4.component';
import { ModalNotstrikeComponent } from './create-tac/modal-notstrike/modal-notstrike.component';
import { DemoUtilsModule } from './create-tac/demo-utils/module';
import { RePagesComponent } from './re-pages/re-pages.component';

import { ToasterModule } from 'angular2-toaster';
import { UiSwitchModule } from 'ngx-toggle-switch';
@NgModule({
  imports: [
    ThemeModule,
    Menu1RoutingModule,
      CalendarModule.forRoot({
          provide: DateAdapter,
          useFactory: adapterFactory,
      }),
      DemoUtilsModule,
      ToasterModule.forRoot(),
      UiSwitchModule,
  ],
  declarations: [
    ...routedComponents,
    CreateTacComponent,
      // ModalsComponent,
      ModalTacComponent,
      SettingTacComponent,
      CalendarComponent,
      Calendar2Component,
      Calendar3Component,
      Calendar4Component,
      ModalNotstrikeComponent,
      RePagesComponent,
  ],
})
export class Menu1Module { }
