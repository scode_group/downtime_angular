import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Calendar4Component } from './calendar4.component';

describe('Calendar4Component', () => {
  let component: Calendar4Component;
  let fixture: ComponentFixture<Calendar4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Calendar4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Calendar4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
