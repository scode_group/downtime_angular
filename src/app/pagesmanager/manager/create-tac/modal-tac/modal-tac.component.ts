import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../../auth.service';
import {forEach} from '@angular/router/src/utils/collection';
import {Router} from '@angular/router';
// import { CreateTacComponent } from '../create-tac.component';

@Component({
  selector: 'ngx-modal',
  templateUrl: './modal-tac.component.html',
  styleUrls: ['./modal-tac.component.scss']
})

export class ModalTacComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal,
              private Auth: AuthService,
              private router: Router) { }

    Date: string;
    Date_end: string;
    allEmp = [];
    today = Date.now();
    hostUrl = this.Auth.host_API();
    // VGA_star: number;
    all_number = [];
    // insert_Emp = {}

    ngOnInit() {
        let push_number = [];
    // console.log (this.Date, ', ', this.Date_end, ', ', this.today)
      this.Auth.allEmp_table().subscribe(data => {
          this.allEmp = (<any>data).users
          let numEmp = 0
          this.allEmp.forEach(function (emp) {
              numEmp++
              // console.log (numEmp, ', ', emp.username)
              const number =  {'num': numEmp, 'number': emp.username, 'emp_id': emp.emp_id, 'value': false};
              push_number.push(number);
          })
          this.all_number = push_number
          console.log (this.all_number)
      });
  }

    closeModal() {
        this.activeModal.close();
    }

    submitModal() {
        // console.log (this.Date, ', ', this.Date_end, ', ', this.today)
        var insertEmp = [];
        this.all_number.forEach (function (value) {
            // this.insert.
            if (value.value) {
                insertEmp.push(value.emp_id);
            }
            console.log (value)
        })
        console.log (insertEmp)
        this.Auth.create_tac(this.Date, this.Date_end, insertEmp).subscribe(data => {
           console.log (<any>data);
        });
        this.router.navigate(['/manager/manager/re-pages', { pages: '/manager/manager/createtac', date: null}])
        this.activeModal.close();
    }

    VGA_star(star1, star2, star3, star4, star5) {
      const VGA = ( star1 + star2 + star3 + star4 + star5) / 5;
      return VGA
    }

    call_birthday(emp_birthday) {
        // const birthday = new Date(emp_birthday);
        // console.log (this.today, ' , ',  Date.parse(emp_birthday))
        const Age = this.today - Date.parse(emp_birthday)
        var num_years = Age/31536000000;
        num_years = Math.floor(num_years)
      return num_years
    }

    selectEmp(i) {
        if (this.all_number[i].value) {
            this.all_number[i].value = false
        }else {
            this.all_number[i].value = true
        }
        console.log (this.all_number[i], ' value=> ', this.all_number[i].value)
    }
}
