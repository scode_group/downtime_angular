import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ModalTacComponent } from './modal-tac/modal-tac.component';
import { SettingTacComponent } from './setting-tac/setting-tac.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../auth.service';

import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'ngx-create-tac',
  templateUrl: './create-tac.component.html',
  styleUrls: ['./create-tac.component.scss']
})
export class CreateTacComponent implements OnInit {

  constructor(private modalService: NgbModal,
              private Auth: AuthService,
              private toasterService: ToasterService) { }


    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'ค่าเริ่มต้น!';
    content = `ค่าเริ่มต้น!`;
    timeout = 5000;
    toastsLimit = 8;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

  formTac: FormGroup;
  // year
    Jyear: any = [
        {number: '2018'},
        {number: '2019'},
        {number: '2020'},
        {number: '2021'},
        {number: '2022'},
    ];

    Nmonths: any = {
        '01': 'มกราคม',
        '02': 'กุมภาพันธ์',
        '03': 'มีนาคม',
        '04': 'เมษายน',
        '05': 'พฤษภาคม',
        '06': 'มิถุนายน',
        '07': 'กรกฎาคม',
        '08': 'สิงหาคม',
        '09': 'กันยายน',
        '10': 'ตุลาคม',
        '11': 'พฤศจิกายน',
        '12': 'ธันวาคม',
    }
    Jmonths: any = [
        {number: '01', name: this.Nmonths['01']},
        {number: '02', name: this.Nmonths['02']},
        {number: '03', name: this.Nmonths['03']},
        {number: '04', name: this.Nmonths['04']},
        {number: '05', name: this.Nmonths['05']},
        {number: '06', name: this.Nmonths['06']},
        {number: '07', name: this.Nmonths['07']},
        {number: '08', name: this.Nmonths['08']},
        {number: '09', name: this.Nmonths['09']},
        {number: '10', name: this.Nmonths['10']},
        {number: '11', name: this.Nmonths['11']},
        {number: '12', name: this.Nmonths['12']},
    ];

    tacs: any = [];
    create_day: number = 1;
    create_month: string = null;
    create_year: string = null;
    month_name: string = 'เลือกเดือน';
    show_month: string = 'เลือกเดือน';
    show_year: string = 'เลือกปี';

    imageUrl: string = '/assets/icon/plus.png';

    today: number = Date.now();
    fixedTimezone = this.today;

    testt = this.Auth.testData;
    num_tac = {num_tac: null};
    data: any = {
        firstday: 1,
        lastday: 15
    }

    setting_Pag: any = {
        'rowsSet': 5,
        'maxNumber': 3,
    }
    numRowsTac: number = 0;
    RowsNumbers: any = [
        { number: 1, start: 0, end: 0 }
    ]
    row_start: number = 0;
    row_end: number = this.setting_Pag.rowsSet;
    row_number: number = 1;
    makeToast() {
        this.showToast(this.type, this.title, this.content);
    }

    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

  submitTac (event) {
  }

  onTacdate(value) {

  }

  selectMonth(month) {
    this.create_month = month.number;
      this.month_name = month.name;
      this.show_month = month.name;
    // console.log('create_month => ', month.number)
  }

    selectYear(year) {
        this.create_year = year.number;
        this.show_year = year.number;
        // console.log('create_year => ', year.number)
    }


    selectDay (value) {
        this.create_day = value;
        // console.log('value => ', value)
    }

    daysInMonth(year, month) {
        var result = new Date(year,month,1,-1).getDate();
        return result;
    }

    onSubmit() {
      let day: string;
        let day_end: string;
        switch (this.create_day) {
            case 1: {
                day = '01';
                day_end = '10';
                break;
            }
            case 2: {
                day = '11';
                day_end = '20';
                break;
            }
            case 3: {
                day = '21';
                day_end = this.daysInMonth(this.create_year, this.create_month).toString();
                break;
            }
            case 0: {
                day = this.data.firstday;
                day_end = this.data.lastday;
                break;
            }
            default: {
                day = null;
                day_end = null;
                break;
            }
        }
        if (this.create_month != null && this.create_year != null && day != null && day_end != null) {
            // console.log(this.daysInMonth(this.create_year, this.create_month));
            const Date = this.create_year + '/' + this.create_month + '/' + day;
            const Date_end = this.create_year + '/' + this.create_month + '/' + day_end;
            // console.log(Date, ', ', Date_end);
            const activeModal = this.modalService.open(ModalTacComponent,{ size: 'lg', container: 'nb-layout' });
            // console.log ('test', activeModal);
            activeModal.componentInstance.Date = Date;
            activeModal.componentInstance.Date_end = Date_end;
        }else {
            this.showToast('error', 'แจ้งเตือน', 'กรุณาเลือก เดือน/ปี');
            console.log ('กรุณาเลือก เดือน/ปี');
        }

    }

    showLargeModal() {
        this.modalService.open(ModalTacComponent);
        // const activeModal = this.modalService.open(ModalComponent);

        // activeModal.componentInstance.modalHeader = 'Large Modal';
    }

    setDate (Date) {
        // return Date
    }

    setYear (Year) {
        this.create_year = Year;
        this.show_year = Year;
        // return Year
    }

    setMonth (Month) {
        this.create_month = Month;
        this.month_name = this.Nmonths[Month];
        console.log ('show_month => ', this.show_month);

        // return Month
    }

    setDay (Day) {
        // return Day
    }

    settingTac() {
        const testtt = this.modalService.open(SettingTacComponent,{ size: 'lg', container: 'nb-layout' });
        console.log ('testtt', testtt);
    }

    testApi2() {
        const date = '2015-11-23';
        const comment = 'ทดสอบ';
        const routine = true;
        const still = true;
        this.Auth.Add_notstrike( date, comment, status, routine, still ).subscribe( data => {

        });
    }

    Ftest( value ) {
      this.testt = value;
    }

    Pagination_click(number, start, end) {
        console.log ('number', number, ' start', start, ' end', end)
        this.row_number = number;
        this.row_start = start;
        this.row_end = end;
    }
    Pagination_back() {
        console.log ('back')
        if (this.row_number > 1) {
            this.row_number = this.row_number - 1;
            this.row_start = this.row_start - this.setting_Pag.rowsSet;
            this.row_end = this.row_end - this.setting_Pag.rowsSet;
        }
        // this.row_number = number;
        // this.row_start = start;
        // this.row_end = end;
    }
    Pagination_next() {
        console.log ('next')
        if (this.row_number < this.numRowsTac ) {
            this.row_number = this.row_number + 1;
            this.row_start = this.row_start + this.setting_Pag.rowsSet;
            this.row_end = this.row_end + this.setting_Pag.rowsSet;
        }
    }

    ngOnInit() {
        // this.showToast('default', 'แจ้งเตือน', 'กำลังเข้าสู่ระบบ');
        this.formTac = new FormGroup({
            // username: new FormControl('', [
            //     Validators.required,
            //     Validators.minLength(9),
            //     Validators.maxLength(15),
            //     Validators.pattern('[0-9 ]*')
            // ]),
            // password: new FormControl('', [
            //     Validators.required,
            //     // Validators.patte
            // rn('[a-zA-Z ]*')
            // ])
        })
        // const fixedTimezone = this.today;
        // console.log (this.fixedTimezone );
        this.Auth.all_tac().subscribe(data => {
            this.tacs = <any>data;
            // console.log (this.tacs);
        });

        this.Auth.num_tac().subscribe(data => {
            this.num_tac = <any>data;
            // console.log (<any>data.num_tac);
            this.numRowsTac = Math.ceil(this.num_tac.num_tac / this.setting_Pag.rowsSet);
            this.RowsNumbers = [];
            const rowsSet = this.setting_Pag.rowsSet;
            let i = 0;
            for (i = 1; i <= this.numRowsTac; i++) {
                const start = (i - 1) * rowsSet;
                const end = i * rowsSet;
                console.log ('number', i, ' start', start, ' end', end)
                this.RowsNumbers.push({number: i, start: start, end: end});
            }
        });
    }
}
