import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../../auth.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { CreateTacComponent } from '../create-tac.component';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-modal-notstrike',
  templateUrl: './modal-notstrike.component.html',
  styleUrls: ['./modal-notstrike.component.scss']
})
export class ModalNotstrikeComponent implements OnInit {

    Date: string;
    notstrikeForm: FormGroup;
    notstrike: boolean = false;
    stopwork: boolean = false;
    notStrike_comment: string = '';
    loader: boolean = false;
    imageUrl: string = this.Auth.loader();
  constructor(private activeModal: NgbActiveModal,
              private Auth: AuthService,
              private router: Router) { }

  submitModal() {

  }
  closeModal() {
      this.activeModal.close();
  }
    fnotstrike(value) {
      this.notstrike = !value;
        this.stopwork = false;
      console.log ('notstrike', this.notstrike)
    }

    fstopwork(value) {
        this.stopwork = !value;
        this.notstrike = false;
        console.log ('stopwork', this.stopwork)
    }

  OnSubmit(event) {
      this.loader = true;
    // console.log ('event =>', event);
      // const target = event.target
      // const inputFirstName = target.querySelector('#inputFirstName').value
      const target = event.target;
      // console.log ('target =>', target);
      const comment = target.querySelector('#comment').value
      console.log ('Date =>', this.Date, 'status =>', this.notstrike, 'comment =>', comment, 'notstrike =>', this.notstrike);
      if (this.notstrike || this.stopwork) {
          let status: number = null;
          if (this.notstrike === true) {
              status = 2
          }else if (this.stopwork === true) {
              status = 1
          }else {
              status = 0
          }
          console.log ('status', status);
          this.Auth.Add_notstrike(this.Date, comment, status, 0, 0).subscribe(data => {
              this.loader = false;
              this.router.navigate(['/manager/manager/re-pages', { pages: '/manager/manager/createtac', date: this.Date}])
              this.activeModal.close();
          });
          // location.reload();

      }else {
          this.Auth.de_notstrike(this.Date).subscribe(data => {
              this.loader = false;
              this.router.navigate(['/manager/manager/re-pages', { pages: '/manager/manager/createtac', date: this.Date}])
              this.activeModal.close();
          });
      }
  }

  ngOnInit() {
      this.notstrikeForm = new FormGroup({

      });
      this.Auth.notstrike_one(this.Date).subscribe(data => {
        const status = (<any>data).status;
        const status_date = status;
          if (status) {
              console.log ('status', (<any>data).tac)
              this.notStrike_comment = (<any>data).tac.notStrike_comment;
              switch ((<any>data).tac.notStrike_status) {
                  case 0: {
                      this.stopwork = false;
                      this.notstrike = false;
                      break
                  }
                  case 1: {
                      this.stopwork = true;
                      this.notstrike = false;
                      break
                  }
                  case 2: {
                      this.stopwork = false;
                      this.notstrike = true;
                      break
                  }
                  default : {
                      this.stopwork = false;
                      this.notstrike = false;
                      break
                  }
              }
              console.log ('status_date', status_date)
          }
      });
      this.Auth.testData = 'brt';
      console.log(' + ', this.Auth.testData);

  }
}
