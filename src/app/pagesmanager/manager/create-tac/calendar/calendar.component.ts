import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ViewEncapsulation,
    OnInit, Output, EventEmitter
} from '@angular/core';
import {
    CalendarEvent,
    CalendarViewPeriod,
    CalendarMonthViewDay,
    CalendarMonthViewBeforeRenderEvent,
    CalendarWeekViewBeforeRenderEvent,
    CalendarDayViewBeforeRenderEvent
} from 'angular-calendar';
import { Subject } from 'rxjs';
import { AuthService } from '../../../../auth.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import { DatePipe } from '@angular/common';
import {ModalNotstrikeComponent} from '../modal-notstrike/modal-notstrike.component';
import { ActivatedRoute } from '@angular/router';

import {
    isSameMonth,
    isSameDay,
    startOfMonth,
    endOfMonth,
    startOfWeek,
    endOfWeek,
    startOfDay,
    endOfDay,
    format
} from 'date-fns';

// import { NavController, NavParma } from 'ionic-angular';


const RED_CELL: 'red-cell' = 'red-cell';
const BLUE_CELL: 'blue-cell' = 'blue-cell';
const YELLOW_CELL: 'yellow-cell' = 'yellow-cell';
const BLACK_CELL: 'black-cell' = 'black-cell';

interface Film {
    id: number;
    title: string;
    release_date: string;
}

@Component({
  selector: 'ngx-calendar',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
  templateUrl: './calendar.component.html',
    styles: [
        `
            .red-cell {
                background-color: #ad2121 !important;
            }

            .blue-cell {
                background-color: blue !important;
            }

            .yellow-cell {
                background-color: #e3ac00 !important;
            }
            .black-cell {
                background-color: #000000 !important;
            }
        `
    ]
})
export class CalendarComponent implements OnInit {

    view: string = 'month';

    colors: any = {
        red: {
            primary: '#ad2121',
            secondary: '#FAE3E3'
        },
        blue: {
            primary: '#1e90ff',
            secondary: '#D1E8FF'
        },
        yellow: {
            primary: '#e3ac00',
            secondary: '#FDF1BA'
        }
    };

    @Output() viewChange: EventEmitter<string> = new EventEmitter();

    @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

    selectDate = '2018-09-01';

    viewDate: Date = new Date();

    events: CalendarEvent[] = [
        // {
        //     title: 'Click me',
        //     color: this.colors.yellow,
        //     start: new Date()
        // },
        // {
        //     title: 'Or click me',
        //     color: this.colors.blue,
        //     start: new Date()
        // }
    ];

    period: CalendarViewPeriod;

    refresh: Subject<any> = new Subject();

    cssClass: string = RED_CELL;

    clickedDate: Date;

    notstrike: any = [];

    selectedMonthViewDay: CalendarMonthViewDay;

    selectedDays: any = [];

    apiData: boolean = true;

    imageUrl: string = this.Auth.loader();

    loader: boolean = true;

    data: any = {}

    m_y: string = null;

    activeDayIsOpen: boolean;

    constructor(private modalService: NgbModal,
                private Auth: AuthService,
                private datePipe: DatePipe,
                private route: ActivatedRoute,
                private cdr: ChangeDetectorRef) { }

    beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
        if ( this.apiData ) {
            // console.log ('viewDate', this.viewDate)
            var datepip2 = this.datePipe.transform(this.viewDate, 'yyyy-MM');
            // console.log ('datepip2', datepip2)
            const date_start = '2018-12-01';
            const date_end = '2018-12-30';
            this.Auth.notstrike( datepip2 ).subscribe( data => {
                const json = <any>data;
                this.notstrike = json.tac;
                // console.log('notstrike =>', this.notstrike);
                this.apiData = false;
                this.refresh.next();
                this.loader = false;
            });
        }else {
            // console.log ('apiData =>', this.apiData)
            body.forEach(day => {
                var date = day.date.toLocaleDateString();
                var datepip2 = this.datePipe.transform(day.date, 'yyyy-MM-dd');
                var splitted = date.split('/', 3);
                var dateOutput = splitted[2] + '-' + splitted[1] + '-' + splitted[0];
                var date2 = day.isToday;
                // console.log(dateOutput, '2561-12-20', this.cssClass)
                this.notstrike.forEach(value => {
                    // console.log (value.notStrike_date, datepip2)
                    if (value.notStrike_date === datepip2) {
                        switch (value.notStrike_status) {
                            case 0: {
                                this.cssClass = BLUE_CELL;
                                break;
                            }
                            case 1: {
                                this.cssClass = RED_CELL;
                                break;
                            }
                            case 2: {
                                this.cssClass = BLACK_CELL;
                                break;
                            }
                        }
                        day.cssClass = this.cssClass;
                        // console.log(' -------*--------- ')
                    }
                });
                // if (dateOutput === '2561-12-20') {
                //     day.cssClass = this.cssClass;
                //     console.log(' -------*--------- ')
                // }
                // if (day.date.getDate() % 2 === 1) {
                //     day.cssClass = this.cssClass;
                // }
                // });
            });
            this.apiData = true;
            this.loader = true;
        }
    }

    test (event) {
        // const date_start = '2018-12-01';
        // const date_end = '2018-12-30';
        // this.Auth.notstrike( date_start, date_end ).subscribe( data => {
        //     const json = <any>data;
        //     this.notstrike = json.tac;
        //     console.log ( 'notstrike =>', this.notstrike );
        //     this.beforeMonthViewRender(event)
        // });
        this.beforeMonthViewRender(event)
    }

    dayClicked(day: CalendarMonthViewDay): void {
        this.clickedDate = day.date;
        // console.log ('clickedDate =>', this.clickedDate);
        this.selectedMonthViewDay = day;
        const selectedDateTime = this.selectedMonthViewDay.date.getTime();
        // console.log ('dayClicked =>', selectedDateTime);
        // console.log ('selectedDays', this.selectedDays);
        const datepip = this.datePipe.transform(day.date, 'yyyy-MM-dd');
        // console.log ('day.date', day.date);
        // const addDay = {date: datepip, format: GREEN_CELL};
        // console.log ('datepip =>', datepip);
        // this.addSetDay = this.compareDate(addDay.date, this.SetDay);
        const activeModal = this.modalService.open(ModalNotstrikeComponent,{ size: 'lg', container: 'nb-layout' });
        // console.log ('test', activeModal);
        activeModal.componentInstance.Date = datepip;
    }

    dayClicked2({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }

    refreshView(): void {
        this.cssClass = this.cssClass === RED_CELL ? BLUE_CELL : RED_CELL;
        this.refresh.next();
    }

    loading() {
        this.loader = true;
        this.m_y = format(this.viewDate, 'MM-YYYY');
        // // this.Auth.loader();
        // console.log (this.loader, this.Auth.load);
        // if (this.Auth.load) {
        //     console.log ('re calender')
        // }
        // return this.loader
    }

    eventClicked(event: CalendarEvent<{ film: Film }>): void {
        window.open(
            `https://www.themoviedb.org/movie/${event.meta.film.id}`,
            '_blank'
        );
    }

    beforeViewRender(
        event:
            | CalendarMonthViewBeforeRenderEvent
            | CalendarWeekViewBeforeRenderEvent
            | CalendarDayViewBeforeRenderEvent
    ) {
        this.period = event.period;
        this.cdr.detectChanges();
    }

    showcomment(Date) {
        const datepip = this.datePipe.transform(Date, 'yyyy-MM-dd');
        // console.log ('Date', datepip, 'Tac', this.notstrike)
        let text = null;
        this.notstrike.forEach(array => {
            if (array.notStrike_date === datepip) {
                text = array.notStrike_comment;
            }
            // console.log ('array', array.notStrike_date)
        })
        return text;
    }

    ngOnInit() {
        this.route.params.subscribe(date => {
            this.data = date
            console.log('date', date, ' ==>', this.data)
            if (this.data.data !== undefined) {
                this.selectDate = this.data.data;
                // console.log('this.selectDate', this.selectDate);
                this.viewDate = new Date(this.selectDate);
            }
        })
        this.m_y = format(this.viewDate, 'MM-YYYY');
        // console.log (this.m_y);
        // this.selectDate = this.data.data;
        // this.viewDate = new Date(this.selectDate);
        // const tset = new Date(this.selectDate);
        // console.log ('Locale ', tset.toLocaleDateString());
        // tset.toLocaleDateString()
    }
}