import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../../auth.service';
import {forEach} from '@angular/router/src/utils/collection';
// import { CreateTacComponent } from '../create-tac.component';

@Component({
  selector: 'ngx-modal',
  templateUrl: './setting-tac.component.html',
  styleUrls: ['./setting-tac.component.scss']
})
export class SettingTacComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal,
              private Auth: AuthService) { }

    data: any = {}
    imageUrl: string = this.Auth.loader();
    loader: boolean = true;

    st_month: number = 0;
    st_tac: number = 0;
    st_alter: number = 0;
    st_delegate: number = 0;


    ngOnInit() {
        this.Auth.task_start().subscribe(data => {
            this.data.st_month = (<any>data).st_month;
            this.data.st_tac = (<any>data).st_tac;
            this.data.st_alter = (<any>data).st_alter;
            this.data.st_delegate = (<any>data).st_delegate;
            this.loader = false;
        })
    }

    submitModal() {
      let st_month;
      let st_tac;
      let st_alter;
      let st_delegate;
        // console.log ('--> fulltime =>', fulltime, ' || temporary =>', temporary, ' || first =>', first,  ' || dalegate =>', dalegate)
        if (this.data.st_month === undefined) {
            st_month = 0;
        }else {
            st_month = this.data.st_month
        }
        if (this.data.st_tac === undefined) {
            st_tac = 0;
        }else {
            st_tac = this.data.st_tac
        }
        if (this.data.st_alter === undefined) {
            st_alter = 0;
        }else {
            st_alter = this.data.st_alter
        }
        if (this.data.st_delegate === undefined) {
            st_delegate = 0;
        }else {
            st_delegate = this.data.st_delegate
        }
        console.log ('st_month =>', st_month, ' || st_tac =>', st_tac, ' || st_alter =>', st_alter,  ' || st_delegate =>', st_delegate)
        // location.reload()
        this.Auth.settask_start(st_month, st_tac, st_alter, st_delegate).subscribe(data => {
            this.activeModal.close();
        })
        // this.Auth.settask_start().subscribe(data => {
        //
        // })
    }
    closeModal() {
        this.activeModal.close();
    }

    testModal() {
      console.log ('data =>', this.data)

    }
}
