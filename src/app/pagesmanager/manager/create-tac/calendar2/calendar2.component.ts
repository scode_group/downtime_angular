import { Component, OnInit , ChangeDetectionStrategy } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';

@Component({
  selector: 'ngx-calendar2',
  templateUrl: './calendar2.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./calendar2.component.scss']
})
export class Calendar2Component implements OnInit {

  constructor() { }

    view: string = 'month';

    viewDate: Date = new Date();

    events: CalendarEvent[] = [];

    clickedDate: Date;

  ngOnInit() {
  }

}
