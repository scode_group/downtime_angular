import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ngx-tac',
  templateUrl: './tac.component.html',
  styleUrls: ['./tac.component.scss']
})
export class TacComponent implements OnInit {

    hostUrl = this.Auth.host_API();
    today = Date.now();
    tac_id: number = null;
    tac_emps: any = null;
    tac: any = {'task_start': null, 'task_end': null};
    enable: boolean = false;

    constructor(private Auth: AuthService,
                private route: ActivatedRoute,
                private router: Router) {
        this.tac_id = this.route.snapshot.params.tac_id;
    }

    ngOnInit() {
        console.log (this.tac_id)
        this.Auth.tac(this.tac_id).subscribe(data => {
            this.tac_emps = (<any>data).tac_emp;
            console.log ('tac : ', this.tac_emps);
            this.tac = (<any>data).tac;
            this.enable = this.tac.task_status;
        });
    }

    call_birthday(emp_birthday) {
        // const birthday = new Date(emp_birthday);
        // console.log (this.today, ' , ',  Date.parse(emp_birthday))
        const Age = this.today - Date.parse(emp_birthday)
        var num_years = Age/31536000000;
        num_years = Math.floor(num_years)
        return num_years
    }

    VGA_star(star1, star2, star3, star4, star5) {
        const VGA = ( star1 + star2 + star3 + star4 + star5) / 5;
        return VGA
    }

    de_tac() {
        this.Auth.de_tac(this.tac_id).subscribe(data => {
            this.router.navigate(['manager/manager/createtac']);
        });
    }

    onChange(event) {
        console.log (event);
        this.Auth.tac_changeStatus(this.tac_id, event).subscribe(data => {

        })

    }

    back() {
        this.router.navigate(['manager/manager/createtac']);
    }
}
