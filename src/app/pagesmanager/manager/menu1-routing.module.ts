import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menu1Component } from './menu1.component';
// import { Submenu1Component } from './submenu1/submenu1.component';
import { AlluserComponent } from './alluser/alluser.component';
// import { Submenu3Component } from './submenu3/submenu3.component';
// import { Submenu4Component } from './submenu4/submenu4.component';
// import { Submenu5Component } from './submenu5/submenu5.component';
import { ListEmpComponent } from './list-emp/list-emp.component';
import { StarComponent } from './star/star.component';
// import { SettingTacComponent } from './setting-tac/setting-tac.component';
// import { PeportComponent } from './peport/peport.component';
// import { UploadComponent } from './upload/upload.component';
import { RefreshingComponent } from './refreshing/refreshing.component';
// import { PositionComponent } from './position/position.component';
import { CreateTacComponent } from './create-tac/create-tac.component';
// import { ModalsComponent } from './modals/modals.component';
import { ModalTacComponent } from './create-tac/modal-tac/modal-tac.component';
import { SettingTacComponent } from './create-tac/setting-tac/setting-tac.component';
import { TacComponent } from './tac/tac.component';
import { ModalNotstrikeComponent } from './create-tac/modal-notstrike/modal-notstrike.component';
import { RePagesComponent } from './re-pages/re-pages.component';

const routes: Routes = [{
  path: '',
  component: Menu1Component,
  children: [
  {
      path: 'listEmp',
      component: ListEmpComponent,
  },
  {
    path: 'star/:index',
    component: StarComponent,
  }, {
      path: 'refresh',
      component: RefreshingComponent,
  },  {
      path: 'alluser',
      component: AlluserComponent,
  },  {
          path: 'createtac',
          component: CreateTacComponent,
  },  {
          path: 'testmodal2',
          component: ModalTacComponent,
  },  {
          path: 'testmodal3',
          component: ModalNotstrikeComponent,
  }, {
          path: 'Setting',
          component: SettingTacComponent,
  }, {
          path: 're-pages',
          component: RePagesComponent,
      },  {
          path: 'tac/:tac_id',
          component: TacComponent,
      }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class Menu1RoutingModule {

}

export const routedComponents = [
  Menu1Component,
    ListEmpComponent,
    StarComponent,
    RefreshingComponent,
    AlluserComponent,
    CreateTacComponent,
    // ModalsComponent,
    ModalTacComponent,
    SettingTacComponent,
    TacComponent,
    ModalNotstrikeComponent,
    RePagesComponent,
];
