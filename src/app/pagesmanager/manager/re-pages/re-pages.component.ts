import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-re-pages',
  templateUrl: './re-pages.component.html',
  styleUrls: ['./re-pages.component.scss']
})
export class RePagesComponent implements OnInit {
    data: any = {}
  constructor(private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
      this.route.params.subscribe(prams => {
          this.data = prams
          console.log(prams)
      })
      // console.log (this.data.date);
      if (this.data.date !== 'null') {
          // console.log (true);
          this.router.navigate([this.data.pages, { data: this.data.date}])
      }else {
          // console.log (false);
          this.router.navigate([this.data.pages])
      }
  }

}
