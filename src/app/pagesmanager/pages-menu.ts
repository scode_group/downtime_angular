import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/manager/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
    {
        title: 'manager',
        icon: 'ion-arrow-down-b',
        children: [
            {
                title: 'สร้าง ID พนักงาน',
                link: '/manager/manager/alluser',
            },
            {
                title: 'รายชื่อพนักงาน',
                link: '/manager/manager/listEmp',
            },
            // {
            //     title: 'รายชื่อพนักงาน',
            //     link: '/manager/manager/listEmp',
            // },
            // {
            //     title: 'ให้ดาว',
            //     link: '/manager/manager/star/0',
            // },
            {
                title: 'สร้างแทค',
                link: '/manager/manager/createtac',
            },
            // {
            //     title: 'Report',
            //     link: '/manager/manager/peport',
            // },
            // {
            //     title: 'upload',
            //     link: '/manager/manager/upload',
            // },
            // {
            //     title: 'refreshing',
            //     link: '/manager/manager/refresh',
            // },
            /*{
                title: 'submenu5',
                link: '/pages/manager/submenu5',
            },*/
        ],
    },
    {
        title: 'logOut',
        icon: 'ion-log-out',
        link: '/logout',
        home: true,
    },
];
