import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
// import {TablesModule} from "./tables/tables.module";
// import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { AlltestComponent } from './alltest/alltest.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
      path: 'alltest',
      component: AlltestComponent,
  },
  {
    path: 'manager',
    loadChildren: './manager/menu1.module#Menu1Module',
  },
      {
          path: 'table',
          loadChildren: './tables/tables.module#TablesModule',
      },
      /*{
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  },*/
      {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
      /*{
    path: '**',
    component: NotFoundComponent,
  }*/
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
