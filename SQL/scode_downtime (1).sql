-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2018 at 11:25 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scode_downtime`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` varchar(10) NOT NULL,
  `admin_pass` int(20) NOT NULL,
  `admin_status` int(1) NOT NULL COMMENT 'สถานะ 1 Employee, 2 Supervisor, 3 Manager, 4 Admin',
  `admin_name` varchar(50) NOT NULL COMMENT 'ชื่อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_pass`, `admin_status`, `admin_name`) VALUES
('0111111111', 1234, 4, 'admin01'),
('0222222222', 1234, 3, 'Manager01');

-- --------------------------------------------------------

--
-- Table structure for table `delegate`
--

CREATE TABLE `delegate` (
  `delegate_id` int(11) NOT NULL COMMENT 'รหัสแทนงาน',
  `delegate_date` date NOT NULL COMMENT 'วันที่แทนงาน',
  `delegate_status` tinyint(1) NOT NULL COMMENT 'สถานะการอนุมัติแทนงาน 0 ไม่อนุมัติ, 1 อนุมัติ',
  `emp_id` int(5) NOT NULL COMMENT 'id พนักงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(5) NOT NULL COMMENT 'id พนักงาน',
  `EnNo` int(5) DEFAULT NULL COMMENT 'รหัสลายนิ้วมือ',
  `emp_sex` int(1) DEFAULT NULL COMMENT 'เพศ 0.หญิง 1.ชาย',
  `emp_name` varchar(30) COLLATE utf8_croatian_ci DEFAULT NULL COMMENT 'ชื่อจริง',
  `emp_lastname` varchar(30) COLLATE utf8_croatian_ci DEFAULT NULL COMMENT 'นามสกุล',
  `emp_nickname` varchar(20) COLLATE utf8_croatian_ci DEFAULT NULL COMMENT 'ขื่อเล่น',
  `emp_position` int(5) DEFAULT NULL,
  `emp_address` text COLLATE utf8_croatian_ci COMMENT 'ที่อยู่',
  `emp_birthday` date DEFAULT NULL COMMENT 'วันเกิด',
  `emp_weight` float(6,2) DEFAULT NULL COMMENT 'น้ำหนัก',
  `emp_height` float(6,2) DEFAULT NULL COMMENT 'ส่วนสูง',
  `emp_img` varchar(50) COLLATE utf8_croatian_ci DEFAULT NULL COMMENT 'ที่อยู่รูปภาพ',
  `emp_routine` tinyint(1) NOT NULL COMMENT 'สถานะ 0.พนักงานชั่วคราว 1.พนักงานประจำ',
  `emp_still` tinyint(1) NOT NULL COMMENT 'สถานะ 0.ไม่ทำงานแล้ว 1.ยังทำงานอยู่',
  `emp_data` tinyint(1) NOT NULL COMMENT 'สถานะเพิ่มข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `EnNo`, `emp_sex`, `emp_name`, `emp_lastname`, `emp_nickname`, `emp_position`, `emp_address`, `emp_birthday`, `emp_weight`, `emp_height`, `emp_img`, `emp_routine`, `emp_still`, `emp_data`) VALUES
(1, NULL, NULL, 'ธนกร', 'เทพสุวรรณ', 'เฟรม', 1, '123/456 ต.เต่า อ.อ่าง จ.จาน', '1996-02-26', 75.00, 175.00, 'img/ZbkTitUDBn2Mda3anmL1qr6IeomhjLZeKrpdVrVZ.png', 1, 1, 1),
(2, NULL, NULL, 'ธนกร2', 'เทพสุวรรณ2', 'เฟรม2', 2, '123/456 ต.เต่า อ.อ่าง จ.จาน', '1996-02-26', 175.00, 75.00, 'img/hfo6MMNyWycT3FTCcha3F2gLtIQ2XRoYVu8FOJwu.png', 1, 1, 1),
(3, NULL, NULL, NULL, NULL, 'Mana', 2, NULL, NULL, NULL, NULL, NULL, 1, 1, 0),
(4, NULL, NULL, NULL, NULL, 'admin', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fingetscan`
--

CREATE TABLE `fingetscan` (
  `scan_id` int(11) NOT NULL COMMENT 'รหัสแสกนลายนิ้วมือ',
  `EnNo` int(5) NOT NULL COMMENT 'รหัสลายนิ้วมือ จากเครื่องแสกน',
  `scan_name` varchar(20) COLLATE utf8_croatian_ci NOT NULL COMMENT 'ชื่อพนักงาน จากเครื่องแสกน',
  `scan_datetime` datetime NOT NULL COMMENT 'เวลาที่แสกน จากเครื่องแสกน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `history_id` int(5) NOT NULL COMMENT 'รหัสประวัติการใช้งาน',
  `history_text` text COLLATE utf8_croatian_ci NOT NULL COMMENT 'บันทึกประวัติการใช้งาน',
  `history_datetime` datetime NOT NULL COMMENT 'เวลาที่บันทึกข้อมูล',
  `history_status` int(1) NOT NULL COMMENT 'สถานะ',
  `user_id` varchar(10) COLLATE utf8_croatian_ci NOT NULL COMMENT 'id ผู้ใช้'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notstrike`
--

CREATE TABLE `notstrike` (
  `notStrike_id` int(11) NOT NULL COMMENT 'ไอดีห้ามหยุด',
  `notStrike_comment` text COLLATE utf8_croatian_ci NOT NULL COMMENT 'สาเหตุที่ห้ามหยุด',
  `notStrike_date` date NOT NULL COMMENT 'วันที่ห้ามหยุด',
  `notStrike_routine` tinyint(1) NOT NULL COMMENT 'ใช้กับพนักงานประจำ 0 ไม่, 1 ใช่',
  `notStrike_still` tinyint(1) NOT NULL COMMENT 'ใช้กับพนักงานชั่วคราว 0 ไม่, 1 .ใช่'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0044b9249a41be0d1d9e7145704a9b33f67630cca0cbecef8abe2b1d84cadc71eaa2f3233b29d1c3', 34, 1, 'MyApp', '[]', 0, '2018-09-03 02:21:33', '2018-09-03 02:21:33', '2019-09-03 09:21:33'),
('017de0cc8e8df1ffe5f9330f002cdb3feea62f64b7b69c4bf305fd1249f1c7e4f6347e135ac0a917', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:39:36', '2018-09-10 21:39:36', '2019-09-11 04:39:36'),
('02b671c26e755eca923fb2aebfba70a96b050499578449485635eed21ddfd87c98f667dea2159b4e', 34, 1, 'MyApp', '[]', 0, '2018-09-12 00:36:56', '2018-09-12 00:36:56', '2019-09-12 07:36:56'),
('02ce5b11ae42ee29aee8ee9df28b2eba4711859cc8d76325d0da78dfee56cb0c2a7d8f2d31cf64ae', 7, 1, 'MyApp', '[]', 0, '2018-09-02 22:21:33', '2018-09-02 22:21:33', '2019-09-03 05:21:33'),
('0331843ab9fc81e5691552ccbfa9a37d550c9eadf538306d7b673968bab6f5ea80e366ded2a298e5', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:00:36', '2018-09-12 22:00:36', '2019-09-13 05:00:36'),
('05a0e3d830c7347d01bdc8a44961517e491839f7b233cc6a52cc34a958ee15a525f8e1914959ced6', 35, 1, 'MyApp', '[]', 0, '2018-09-12 21:18:56', '2018-09-12 21:18:56', '2019-09-13 04:18:56'),
('06b4923166d0594e484ca88e6c7f16b7d04656bd1d015e8e84583aa407a23f102bb5ee1af0573df9', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:10:47', '2018-09-10 22:10:47', '2019-09-11 05:10:47'),
('08e620c26a9e6dcd82a71f740f01e0b94be0e2bc2057e26e72a4640db549744716ccccc645061c7d', 36, 1, 'MyApp', '[]', 0, '2018-09-03 20:48:17', '2018-09-03 20:48:17', '2019-09-04 03:48:17'),
('090fa13ca12f4eb0af7c6d893335a8234a2441ca7a29ebad04f59946b4b47cd7706ccc737cf9af33', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:26:03', '2018-09-10 22:26:03', '2019-09-11 05:26:03'),
('0d202cd3c82fe470a84bcbe88255d844ea8a965e0f05db2535949659613ba2a79b1fd399b2220221', 22, 1, 'MyApp', '[]', 0, '2018-09-03 01:16:34', '2018-09-03 01:16:34', '2019-09-03 08:16:34'),
('1061afb56f82e8b0ed560ac3f6c0fc3b43096fd34b301c6944c8ede42811dea36b88eb4847dc4399', 35, 1, 'MyApp', '[]', 0, '2018-09-11 21:42:50', '2018-09-11 21:42:50', '2019-09-12 04:42:50'),
('11719feefd24a6dfdf338e75102ef7cc684f9b6409736f23fd0cc55f0e4dfc05da135d56ce269e28', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:52:53', '2018-09-10 21:52:53', '2019-09-11 04:52:53'),
('1677284f6a45e7b0251369220ca6402277e54e13393a306a3a9e315b229402af1eff006291783bd5', 12, 1, 'MyApp', '[]', 0, '2018-09-03 00:02:17', '2018-09-03 00:02:17', '2019-09-03 07:02:17'),
('17ab88776d8dad450c6167908d8d7460be109919a7e5b88a2fbd97da39ccbb69ac8b8204aa6a2006', 6, 1, 'MyApp', '[]', 0, '2018-09-02 22:12:36', '2018-09-02 22:12:36', '2019-09-03 05:12:36'),
('1aed3aceb0be9e3de3884257d2603e2dd0e2d6401e7e7584887f3201f05084b6cf430c38f6336e0c', 37, 1, 'MyApp', '[]', 0, '2018-09-12 22:29:53', '2018-09-12 22:29:53', '2019-09-13 05:29:53'),
('1c0d0a2a39983ccbee5dfd97de98aad483049676d15490960bb133cf3e0b5cbf45f3cafa488e16d0', 26, 1, 'MyApp', '[]', 0, '2018-09-03 01:24:23', '2018-09-03 01:24:23', '2019-09-03 08:24:23'),
('1c3fd5d62b65dfad6e2e57ec8616856523e73a3ccad47fd4700acb06b24b1603765e90215586923b', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:52:10', '2018-09-10 21:52:10', '2019-09-11 04:52:10'),
('1ecd2c32c8b98dcb7247dd9c7f4aee87220f7e0591e93227ace8ef29e229d2ebd383c680a4e88b10', 38, 1, 'MyApp', '[]', 0, '2018-09-03 02:58:34', '2018-09-03 02:58:34', '2019-09-03 09:58:34'),
('20ea171e92964cbdb1cc45a473f7099775b5f56417329d75e825abc701e9afb3ea70b47aae030f2f', 37, 1, 'MyApp', '[]', 0, '2018-09-12 02:34:09', '2018-09-12 02:34:09', '2019-09-12 09:34:09'),
('21a8247859cd624cf84e4a28dc2bacf4a5927b86bf404f59e6c9bec120c3f3729ff537fdf5cddc30', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:44:05', '2018-09-12 21:44:05', '2019-09-13 04:44:05'),
('23991478a5a97f68ef10c52ad4b359bc413718ff13fa049579987cbc608a1bc36249773a2c984f32', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:51:07', '2018-09-10 21:51:07', '2019-09-11 04:51:07'),
('2566f89d921318557dff68e6a4e63364f90d6ca29b02703d693ac52ce0503341612bbfad4a6d9446', 35, 1, 'MyApp', '[]', 0, '2018-09-12 22:21:26', '2018-09-12 22:21:26', '2019-09-13 05:21:26'),
('25e64d49223521ee12f526fed7f16c633acdb11b6c5bdc5b54921c8a915a85f70940e1b5812c74ec', 31, 1, 'MyApp', '[]', 0, '2018-09-03 01:33:04', '2018-09-03 01:33:04', '2019-09-03 08:33:04'),
('2a7ecedaf6121436c7f247503a946b39710123122f86eb2ae246305dd2771bf77909984662b85d28', 37, 1, 'MyApp', '[]', 0, '2018-09-03 02:43:01', '2018-09-03 02:43:01', '2019-09-03 09:43:01'),
('2bb3ccb9b0e77425acf6de77d66c34ed67da8fed02c90eddfd9508c6f72399402a351e3d3c813345', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:23:05', '2018-09-12 22:23:05', '2019-09-13 05:23:05'),
('2bddef93b200476201a487f3725bd38bf01e32e5b1f799aa122f01475b0e1f7a2bf5b3050e82b3a4', 34, 1, 'MyApp', '[]', 0, '2018-09-12 02:33:29', '2018-09-12 02:33:29', '2019-09-12 09:33:29'),
('2d477670ea4351e0f25b5f9bb9c11d14962f82eec630369d8be787241f5499d2b7c2bb63331353e4', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:50:42', '2018-09-10 21:50:42', '2019-09-11 04:50:42'),
('2f2a1d34f2d8fce1884b95d8477af726c5ea02f115435341d0c8a573af7461ba6e368a6c4196731f', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:17:54', '2018-09-10 22:17:54', '2019-09-11 05:17:54'),
('30a5aaae0a30ac893d53ad63188c0ef2da349ee07e117ef240c8f5223c608b89dd037062c7a67bfe', 37, 1, 'MyApp', '[]', 0, '2018-09-18 01:15:12', '2018-09-18 01:15:12', '2019-09-18 08:15:12'),
('33ca98357098471ca886ae0e8f883523b6397746b0ca8645b35f0d3933837db80c813334346e49d7', 37, 1, 'MyApp', '[]', 0, '2018-09-12 02:34:35', '2018-09-12 02:34:35', '2019-09-12 09:34:35'),
('33ccb543821d059076ba7f1cf9cc8992462421d236fc1f02ae2147a20d9bf34441bc4b860ac09f53', 37, 1, 'MyApp', '[]', 0, '2018-09-11 21:40:33', '2018-09-11 21:40:33', '2019-09-12 04:40:33'),
('35ef79c2aa155fcdb9149d6fd22af309fef9deae7732c1ceb6b7f0c0618e4c9355c9f2a24303ab7a', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:57:54', '2018-09-12 21:57:54', '2019-09-13 04:57:54'),
('36f3cacea1e7cad1832829ace43b17078074575c42b126d480fae1079ddb1fc1f27b62bf3a2f8773', 34, 1, 'MyApp', '[]', 0, '2018-09-12 02:34:55', '2018-09-12 02:34:55', '2019-09-12 09:34:55'),
('37064e48788335c1cf33bc5b7ce63fc009308b8c7828897b7f6016e50b26a120bac40f25a2bb18ff', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:10:31', '2018-09-10 22:10:31', '2019-09-11 05:10:31'),
('3b726f37e1b5c5b6746639dfa9ad6bb3c95ed09bd7c3642daa8f3706e0cf5b1ec75a4ae0357bc370', 37, 1, 'MyApp', '[]', 0, '2018-09-03 03:22:14', '2018-09-03 03:22:14', '2019-09-03 10:22:14'),
('3d07c93171d6bc15b13bb4f2bfd9790475574eb97aeca359ebe7521abe6d16bb9378aa502b33538c', 36, 1, 'MyApp', '[]', 0, '2018-09-11 21:38:33', '2018-09-11 21:38:33', '2019-09-12 04:38:33'),
('405ab5a6766093d9ab6e848c95e365691430ec8c1569fd45ac53e67a2681037ff56e192a2b44489f', 11, 1, 'MyApp', '[]', 0, '2018-09-02 23:52:53', '2018-09-02 23:52:53', '2019-09-03 06:52:53'),
('406de90c7c6e8e41e738365eb78734c1ed807c782528c60f2d83c44a379a7dbcaf149adbcc6572a2', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:06:23', '2018-09-10 22:06:23', '2019-09-11 05:06:23'),
('40a4b28b90a7dc6e31fc87140b17846eb3c0bce2186c1d7a993366dce0ad16e8a61fca7eea18eb39', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:08:48', '2018-09-10 22:08:48', '2019-09-11 05:08:48'),
('415b6e8f4729e27542e59c5bf1c28794c78bacffbea216fecf0a07e84cba4c9b036bcbec86619236', 35, 1, 'MyApp', '[]', 0, '2018-09-03 20:51:31', '2018-09-03 20:51:31', '2019-09-04 03:51:31'),
('42037d4c025b9904bef9d49d9a22596db078741bae9b61760fe368df5f9bddd49b08cad77314dda5', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:00:08', '2018-09-12 22:00:08', '2019-09-13 05:00:08'),
('44b781392f6890c7dbd04d69074bc3375767cbb00fce0dfe3b82cd6eb7c26222b367cda3d4506344', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:03:36', '2018-09-12 22:03:36', '2019-09-13 05:03:36'),
('45c2745c7f0d7f7cf3acf05e22cec1b412c1864dad26d96631a17faa600b2a25718eda2776a8645e', 16, 1, 'MyApp', '[]', 0, '2018-09-03 00:55:47', '2018-09-03 00:55:47', '2019-09-03 07:55:47'),
('4cea4147253a00dc9355d211ba99ef96dbfebc63300c24b4be22ff96876efd7c73fcbe1a78e62e31', 36, 1, 'MyApp', '[]', 0, '2018-09-03 02:30:35', '2018-09-03 02:30:35', '2019-09-03 09:30:35'),
('4f6d67e009f6d92430663304017a03dc42390657f507b47c2d4b3345435d16fab8e59d5c65560b82', 37, 1, 'MyApp', '[]', 0, '2018-09-10 20:50:25', '2018-09-10 20:50:25', '2019-09-11 03:50:25'),
('4fb9e01b2b6704ae8adcda96117aae4874280326324c0fb0e9fabce5c7334d858eacbf932b151acf', 35, 1, 'MyApp', '[]', 0, '2018-09-12 22:21:42', '2018-09-12 22:21:42', '2019-09-13 05:21:42'),
('52460cf3df7e99210116affedd0797c83b69a5d51f7d9ee3a7dd7d8a1ce2d28c715caa80c419f36c', 37, 1, 'MyApp', '[]', 0, '2018-09-12 00:39:44', '2018-09-12 00:39:44', '2019-09-12 07:39:44'),
('56fe5d8992956dd085a1dab3b4a51e48909f78e5c527677813a382bc8bb9513c0a0f21bfb00b6547', 36, 1, 'MyApp', '[]', 0, '2018-09-11 21:18:47', '2018-09-11 21:18:47', '2019-09-12 04:18:47'),
('5742ce36527768bcfa80196a81b3becb4d25161b1839b252d06ea283a37649740ac90004d08afbed', 36, 1, 'MyApp', '[]', 0, '2018-09-11 20:59:21', '2018-09-11 20:59:21', '2019-09-12 03:59:21'),
('594b9480fcd3dda3467ef80eb5fe3e6b69956673b6b356261243fe62e4a7ad25d3e194378491dfd3', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:20:04', '2018-09-12 21:20:04', '2019-09-13 04:20:04'),
('599dc047226ffe169949b628a4050791f5cd994365685e3d06ec55a146e16846289732031c40c78b', 30, 1, 'MyApp', '[]', 0, '2018-09-03 01:31:11', '2018-09-03 01:31:11', '2019-09-03 08:31:11'),
('59e869b44d3ccb9dd51923c4000efdf7033a1b1dc6f5f78b4ce1bc389194471270f7a7317430e45e', 34, 1, 'MyApp', '[]', 0, '2018-09-03 20:51:14', '2018-09-03 20:51:14', '2019-09-04 03:51:14'),
('5a2c1e53e23722bc2e2f98ae27447fa6dacff08e9711e3fa64610a14c2a37c257b44e068f5bcfdcd', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:01:21', '2018-09-12 22:01:21', '2019-09-13 05:01:21'),
('5a3f658f18f20f3f6c1c0e69d4afd7096c23c1b2284755abd1ba4f5e9054db33e6544eff1590c021', 37, 1, 'MyApp', '[]', 0, '2018-09-18 00:20:46', '2018-09-18 00:20:46', '2019-09-18 07:20:46'),
('5a8dedd24639f650bd7f1c70c8b96619285471761a17cef7e9d5cd6d823f1c2dcf2478104adfcc0d', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:18:22', '2018-09-12 21:18:22', '2019-09-13 04:18:22'),
('5b5917e3c85e9efc4fd8d84b8704fab32bf2d700f7dcaf42f26e7d9426af47f12ca016233e69e1f0', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:59:13', '2018-09-10 21:59:13', '2019-09-11 04:59:13'),
('5e18390ea5f0ac75e869bcf4e96d5b87ca02ca17514f95ef0125fadc85a465fcfe821375596ee02f', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:44:14', '2018-09-12 21:44:14', '2019-09-13 04:44:14'),
('5e4963174d90cf2898b2f7293e887be08f83f673532b9b4f5c8143cf20b272f2c2b423c4749d6fdc', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:48:28', '2018-09-10 21:48:28', '2019-09-11 04:48:28'),
('5e6e47e595c5449bd9d171ff76e2f607cde4acefd11afc97891df3e2fc8d4aef5a6f3ed2c98f08c1', 36, 1, 'MyApp', '[]', 0, '2018-09-03 03:21:21', '2018-09-03 03:21:21', '2019-09-03 10:21:21'),
('60f1feb682fb7d3d915dc9acfd1324afc27a781e59ec8c296e472f71b964b5c41ce346e8e582b1a6', 28, 1, 'MyApp', '[]', 0, '2018-09-03 01:29:07', '2018-09-03 01:29:07', '2019-09-03 08:29:07'),
('636a49c4aa571b7d26ac9a7eb0867d41b2de0b5cfb1ce0df0425cc684cf3328b75f924e11af74f59', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:41:39', '2018-09-12 21:41:39', '2019-09-13 04:41:39'),
('64bbce97cd20a9801d19a1939f11cb7b33126c0df7717d028766b94a506118eb13c4b05042501bcc', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:08:58', '2018-09-12 21:08:58', '2019-09-13 04:08:58'),
('67840cca7bc7d4cf1e0686a903063c3e5ac544d7818305b4100be81dc4fb4ae3d73a5c5659c715e7', 32, 1, 'MyApp', '[]', 0, '2018-09-03 01:34:08', '2018-09-03 01:34:08', '2019-09-03 08:34:08'),
('6863bdf76776c59b98da013e6b554ea78978f3ec2c9d0edbd48d889c1e2214341fea8f1c5b816299', 37, 1, 'MyApp', '[]', 0, '2018-09-11 21:40:17', '2018-09-11 21:40:17', '2019-09-12 04:40:17'),
('69537cbd1f5b1b3504f6445d8a7292cabbf5d9006f6c482a1978edc1b18f0f8e17ab5f136899472e', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:56:55', '2018-09-10 21:56:55', '2019-09-11 04:56:55'),
('69bbfd0d56fdf178561b614631997a9d3a4495185d1f706d0cc6df422cc7ba020cac07a32c3ae510', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:51:36', '2018-09-10 21:51:36', '2019-09-11 04:51:36'),
('6aa2002c545cd711fb2471c77f998eaa34b0cabab96cbdb89650332259e2481f9036bf17a49397aa', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:42:46', '2018-09-12 21:42:46', '2019-09-13 04:42:46'),
('6d662832a3e9a8ca335e86c992b71b3b75d1e0e12de803593d749dcba26d3eb3a9d39ca4fdea82f2', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:43:38', '2018-09-12 21:43:38', '2019-09-13 04:43:38'),
('6f083fa182dd4624f7e3765106fd7e015856ebbc19e0a5db168b61df5aecde4fe61b873b673d424c', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:45:31', '2018-09-10 21:45:31', '2019-09-11 04:45:31'),
('702cf3db84847f6728f45154fb5c2646ddaf1be4689a92af5d14bc0c9b36a7a7f9564e72480fcf8b', 9, 1, 'MyApp', '[]', 0, '2018-09-02 23:46:27', '2018-09-02 23:46:27', '2019-09-03 06:46:27'),
('73e02af0fb97f40a208a43637b78c60fe82036a1afe51cc82e57b0ad51fd8dd951af5cab49e37fe1', 34, 1, 'MyApp', '[]', 0, '2018-09-12 00:48:14', '2018-09-12 00:48:14', '2019-09-12 07:48:14'),
('7b1548a7b9cac2d82ead2483779723ff3293135b024eac57157beae89e6d99eee177fe3279cc3b29', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:18:25', '2018-09-10 22:18:25', '2019-09-11 05:18:25'),
('7b163b4f8dec0a5368fe64cc4af1d03d11044cc68cf71626f0d68bbfd8fd98de06bdc4de7e679351', 37, 1, 'MyApp', '[]', 0, '2018-09-16 21:49:54', '2018-09-16 21:49:54', '2019-09-17 04:49:54'),
('7c7793bfd09633d0db4b93fcbb0ce449269c7fd86dd654a613774eeff1b88b53e58e36895ea71e46', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:02:26', '2018-09-12 22:02:26', '2019-09-13 05:02:26'),
('7cd7c5b2c1c01583c73e49d6e28736ecaa691f681caf2f23d6946b25fab00d7512750053d9f1aeaa', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:48:11', '2018-09-10 21:48:11', '2019-09-11 04:48:11'),
('7f7aa0e3193861b7b549670c7ce9b9a51423b61102948159d6b3713fb4ab24f00873f3c8a4a7cbfb', 13, 1, 'MyApp', '[]', 0, '2018-09-03 00:15:36', '2018-09-03 00:15:36', '2019-09-03 07:15:36'),
('8114a8dbeb473d785bcb7c7276f1fbfa909810ab57f7452a895d17cd4425de6bb815c5975e428845', 15, 1, 'MyApp', '[]', 0, '2018-09-03 00:55:27', '2018-09-03 00:55:27', '2019-09-03 07:55:27'),
('812cb571815902c0d81d45171f0be58da90c3c1cdbef54c5552c156106525576662eca8bf45fd426', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:00:54', '2018-09-12 22:00:54', '2019-09-13 05:00:54'),
('816241aa92a8d2f4020e04b55419a3e1d72f6624d41d673c21a2a99857f445dced7f5e1f65b03295', 35, 1, 'MyApp', '[]', 0, '2018-09-03 02:35:49', '2018-09-03 02:35:49', '2019-09-03 09:35:49'),
('81801849c55571a3f27ab66187b15da467d6f472c6a37fdef575d673f45956825a73e3011936c7b1', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:26:11', '2018-09-10 22:26:11', '2019-09-11 05:26:11'),
('842337dc396a2accc2f9cf1c3444792379ea300d19ed0f4a37048404a45f064df6bf77179707ddf7', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:57:08', '2018-09-10 21:57:08', '2019-09-11 04:57:08'),
('860068a2d1283717e475bbe5986d3145b7b9e771d55d0413aa656535b2c7a1c57df4e3c807f48e2f', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:44:12', '2018-09-10 21:44:12', '2019-09-11 04:44:12'),
('8a3bedb16f0f6d46bd248a4a3853a971513165c476a42b86f8f165e969990c08412dcffc43d190aa', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:55:27', '2018-09-10 21:55:27', '2019-09-11 04:55:27'),
('8af2a08ac9ffea10a122752abe787f759a3dfe549b5d707f11f57241fdd2ce2f52e9f423d659eeaa', 36, 1, 'MyApp', '[]', 0, '2018-09-18 01:15:23', '2018-09-18 01:15:23', '2019-09-18 08:15:23'),
('8b5920a5ab51bf8ca11df112b4d8497df68ace073b6b9b2e0666dc3b239672812bd9bed9cae77197', 34, 1, 'MyApp', '[]', 0, '2018-09-09 21:09:49', '2018-09-09 21:09:49', '2019-09-10 04:09:49'),
('91e06d7f448a25a31f4a1d6bafeffc86d15e8808a75ace2ee471a65a495a0f8b7cecb65f7e50575b', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:10:19', '2018-09-10 22:10:19', '2019-09-11 05:10:19'),
('9492dec3bb8da18fb3808cc0226908fdf65981bab07f0723325f37e983fb37f042deb58b17d075a5', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:10:00', '2018-09-10 22:10:00', '2019-09-11 05:10:00'),
('96604b178cc6ba7e1383002440e0959d7dd3b6ff44366a0b49ab7cb55fce790bbfde57a7af707810', 37, 1, 'MyApp', '[]', 0, '2018-09-12 02:35:11', '2018-09-12 02:35:11', '2019-09-12 09:35:11'),
('97cdf5c6b0967f3012e2bc3ed7886200929e37fec1921fc9a570aed1249a408015eab94dd3151334', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:17:53', '2018-09-10 22:17:53', '2019-09-11 05:17:53'),
('97df64c3d527c83ac3afad19d96508fc9c2095f098db189fe4d51b027611e869c73ed39ca53c1fa9', 25, 1, 'MyApp', '[]', 0, '2018-09-03 01:23:44', '2018-09-03 01:23:44', '2019-09-03 08:23:44'),
('9abf27cfcc4c1919284ad04670ab46d035466ae558b9193d0a1e8d902e7c22a4009ab83d7579fbd3', 34, 1, 'MyApp', '[]', 0, '2018-09-03 02:25:05', '2018-09-03 02:25:05', '2019-09-03 09:25:05'),
('9c03a60692197914ac82293f3ea57e7da37b119545fa1106a960d820fadcbaf1d1db2581c246666f', 37, 1, 'MyApp', '[]', 0, '2018-09-09 22:16:08', '2018-09-09 22:16:08', '2019-09-10 05:16:08'),
('9ce0a1a5175020ec38a037defa981fa4784360816f2a1ce4ce395f32119cc6dff7bbdc03ca264cbf', 10, 1, 'MyApp', '[]', 0, '2018-09-02 23:46:48', '2018-09-02 23:46:48', '2019-09-03 06:46:48'),
('9ef7746347a1ab9c47da692719f5e28ec899f722c2d3ad8c9c0f30994d3e4c56d54e7fc3211edd2c', 17, 1, 'MyApp', '[]', 0, '2018-09-03 01:00:10', '2018-09-03 01:00:10', '2019-09-03 08:00:10'),
('a072d2b0714d974e204d56588e7b0ffc89dd96b8fa78cd9e321aff9a6eeca50b5a4f6d161577c5df', 37, 1, 'MyApp', '[]', 0, '2018-09-03 20:55:18', '2018-09-03 20:55:18', '2019-09-04 03:55:18'),
('a0f00e9ece3541fa75ed093b51cbe81fc4f36de0d36f474583baef1d58fb9e4a4bfe91f513263c5c', 36, 1, 'MyApp', '[]', 0, '2018-09-11 21:22:54', '2018-09-11 21:22:54', '2019-09-12 04:22:54'),
('a1ffa6ba12abec90ead1e26be9f087278443ced47f0f184c7c03716157c4ab678d393fb751a8af52', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:08:04', '2018-09-10 22:08:04', '2019-09-11 05:08:04'),
('a37fe3b981bd6f368e806f8a5357cf7feae5942b2dfeb31d92503d38a23ffe9156bcfc37e4e6d0c1', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:46:31', '2018-09-10 21:46:31', '2019-09-11 04:46:31'),
('a38bc583a98a3b93cb43897723f7749210c9f84b6c40a99fc42c774f6ff2a6402bd7007e91b6efe2', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:57:49', '2018-09-10 21:57:49', '2019-09-11 04:57:49'),
('a5d7d4d85f054ee41603bb2d562d841a9a14b836b3e066aadb346e8f20143a077e56f2bc00c84772', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:52:33', '2018-09-10 21:52:33', '2019-09-11 04:52:33'),
('a6f28eed4d887e94cfad0545b28cebb2f724a324438970e9874b93bfbc7dbe7c4fa1eebd73ac260a', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:00:36', '2018-09-12 21:00:36', '2019-09-13 04:00:36'),
('a900b880c02cc92895eb7e7d280b751cedde4e0fd0cbdaada7d6a00804fa9bd7910e110856f2da53', 37, 1, 'MyApp', '[]', 0, '2018-09-11 21:52:45', '2018-09-11 21:52:45', '2019-09-12 04:52:45'),
('aaf2c41cf082abeec7d2e3acb81ba0520e8da4823cc96c5770b7681736a6df380eef62623b0706fd', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:00:02', '2018-09-10 22:00:02', '2019-09-11 05:00:02'),
('ac16304c5ab616fd2459f4bb2a05b946300d3884b65ee646ab5dc8f4af773f269096d41a8d6194f5', 36, 1, 'MyApp', '[]', 0, '2018-09-11 21:23:07', '2018-09-11 21:23:07', '2019-09-12 04:23:07'),
('ada307c6c983baf2b8c79bd4c936c9c5faee0ad8bbfa2449098cf7faba971134d7d378cb0ce47f71', 36, 1, 'MyApp', '[]', 0, '2018-09-18 00:21:11', '2018-09-18 00:21:11', '2019-09-18 07:21:11'),
('af73385de18bb103bef1de5bb8df024734d31bbf79b1eb1b89287cddd6d477ef23fe60079dd6c1d5', 33, 1, 'MyApp', '[]', 0, '2018-09-03 01:54:21', '2018-09-03 01:54:21', '2019-09-03 08:54:21'),
('afe78731ee2c509068dd5cfb99f4bbc2671ae2f7f4add9d1e6d4799baaa26ca7e06c191046c837fe', 36, 1, 'MyApp', '[]', 0, '2018-09-11 21:23:29', '2018-09-11 21:23:29', '2019-09-12 04:23:29'),
('b1cd7262d6b74876a95fc69e89a57e3da4d8431b429fbd0a7da564337fb5d3c5931fc733e52ebeeb', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:54:43', '2018-09-10 21:54:43', '2019-09-11 04:54:43'),
('b20c6c1396523324cdcf72b1538e6c3358fe6a9cba53170ccb2704e204134b400baba2db8897e5fb', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:09:47', '2018-09-12 21:09:47', '2019-09-13 04:09:47'),
('b732360eee4f9234480e6a41b1bf55edfec486a103f314d327ee22473b88cc0fa6da95904d59df7a', 38, 1, 'MyApp', '[]', 0, '2018-09-03 02:57:56', '2018-09-03 02:57:56', '2019-09-03 09:57:56'),
('b7d601249310f6fc5b5c13855bfc0d9c1da959173d1553b4b4d29a07160814370b61441347ed08a8', 36, 1, 'MyApp', '[]', 0, '2018-09-10 20:50:10', '2018-09-10 20:50:10', '2019-09-11 03:50:10'),
('b9527e34da410766a2a0775099c8b6d8b65c0b6cc91ce84017d524c724c1ef9f7cf99e883c02c121', 34, 1, 'MyApp', '[]', 0, '2018-09-11 21:42:36', '2018-09-11 21:42:36', '2019-09-12 04:42:36'),
('bb8f4ed956b52b969a48b2b2968edc186ffee08adcd48d1f1e542f461e3555a10fba3327373d253a', 35, 1, 'MyApp', '[]', 0, '2018-09-03 02:21:48', '2018-09-03 02:21:48', '2019-09-03 09:21:48'),
('bcf291694f347b7c9704e6422d9583f74ca1c0b0feabf37d66cef5a2a73b17b1e1475dc81329765a', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:09:56', '2018-09-10 22:09:56', '2019-09-11 05:09:56'),
('bd23867ec02fe3a609210bbca90a2cf7921721d968633fd88b6bee57bfea3ae1692261e02d702527', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:15:53', '2018-09-10 22:15:53', '2019-09-11 05:15:53'),
('bf80ef58ef87ebbf8c44e1f4d4219994b5db5e1510c9ae85c67d275414071814942b0cdaea8c5b7e', 37, 1, 'MyApp', '[]', 0, '2018-09-18 00:15:02', '2018-09-18 00:15:02', '2019-09-18 07:15:02'),
('c2cae19ad240c69e98fefbacc01706975ad1fb3192ef42ea7be78cdfc2ef52be89ccadeeae60941a', 23, 1, 'MyApp', '[]', 0, '2018-09-03 01:18:26', '2018-09-03 01:18:26', '2019-09-03 08:18:26'),
('c4af9506ce3728c594d88fa85482682e88e0df7e2ec601202e87b5bae8e92afc99b0c394395edddf', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:17:50', '2018-09-10 22:17:50', '2019-09-11 05:17:50'),
('c4ff8da24c6d3947d1f5b01ce6610b1d2f38d4dc3c66efc12b4c81510ce93ca7fa090d46b59cf66b', 37, 1, 'MyApp', '[]', 0, '2018-09-12 00:48:31', '2018-09-12 00:48:31', '2019-09-12 07:48:31'),
('c6c3e60e8528a257b6d6a8426fbee8481ff1b528b0938d3d56c1e8961cf8d228055362662a37db7f', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:56:25', '2018-09-10 21:56:25', '2019-09-11 04:56:25'),
('c880d93f26626d02c4169603cf9f37166557f57782ae5e0f9732f5be1dcd842e62fcbcc6598b80fd', 35, 1, 'MyApp', '[]', 0, '2018-09-12 02:33:40', '2018-09-12 02:33:40', '2019-09-12 09:33:40'),
('c96d77e4b06af69276980875405182cb64f457894e4396fbc05ed10aafbe3d66dc55d22244656681', 37, 1, 'MyApp', '[]', 0, '2018-09-09 20:56:20', '2018-09-09 20:56:20', '2019-09-10 03:56:20'),
('ca6d82d110f1330b28e1335cebe99dd1727c632937a335d9e349b5469d36fb550113a24eccd30c2a', 36, 1, 'MyApp', '[]', 0, '2018-09-03 03:01:38', '2018-09-03 03:01:38', '2019-09-03 10:01:38'),
('cb1b429c1f20ebdf6e89f4c00e574f6a4bdbe4bcf42f49dc446c09c1f71fed14b725e48a68fb062a', 18, 1, 'MyApp', '[]', 0, '2018-09-03 01:14:50', '2018-09-03 01:14:50', '2019-09-03 08:14:50'),
('cbfd1df5b3d3485de10aee3c486e4de7191c82e534981a0ed1f1065b0a2824ae612fcbc7e7605e83', 35, 1, 'MyApp', '[]', 0, '2018-09-10 20:49:49', '2018-09-10 20:49:49', '2019-09-11 03:49:49'),
('cce318f9b980c3075d8467f60f02f6b008c1375a7578a77dd1cc6276e95a9f5070d314669086a9b0', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:08:56', '2018-09-10 22:08:56', '2019-09-11 05:08:56'),
('cdc10de9c51ac79c5fb065259a7de70c7bc312c8840f2d9895b6abb62342d1db0280c303e68ee6d7', 36, 1, 'MyApp', '[]', 0, '2018-09-12 02:33:48', '2018-09-12 02:33:48', '2019-09-12 09:33:48'),
('cf3888135c93dae8389d2a344162684b54adb99fd8829d91f686f7e9a1273415055c59a9267aca95', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:56:02', '2018-09-10 21:56:02', '2019-09-11 04:56:02'),
('d12e5888a9a7dddbcae06f30136c1e26e9a71087a9b5b5fa5471fccfc2a82c9d2b45597ffa365b55', 24, 1, 'MyApp', '[]', 0, '2018-09-03 01:19:37', '2018-09-03 01:19:37', '2019-09-03 08:19:37'),
('d1eabc3af4ee39acadc660bce704c074deab9250e4807ef6b7a9a5bbc4b27d9b1942f2f2f4e9b982', 37, 1, 'MyApp', '[]', 0, '2018-09-03 02:22:59', '2018-09-03 02:22:59', '2019-09-03 09:22:59'),
('d39839e4e1745224856076a1ac181058938a09629928f4571a7a606089f83a20e95c3a44a9ef6d77', 14, 1, 'MyApp', '[]', 0, '2018-09-03 00:15:53', '2018-09-03 00:15:53', '2019-09-03 07:15:53'),
('d59195340926e3e3b7f57f9c2ae447f9acbd8817eef553f2c8ea853daec0c1fefdf6dc8e893755d4', 29, 1, 'MyApp', '[]', 0, '2018-09-03 01:29:58', '2018-09-03 01:29:58', '2019-09-03 08:29:58'),
('d76bfa5fa0b1ecaff827b379b2555807ce0cf3eefe7e132a847ac78202d31315a0753004c19e3c27', 37, 1, 'MyApp', '[]', 0, '2018-09-18 01:23:48', '2018-09-18 01:23:48', '2019-09-18 08:23:48'),
('d95688b711a2f1c0a3da9ead6d417c09b6ecc5187c11708e8e174f8c5d57cdcb430010e9d89adadd', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:26:40', '2018-09-10 22:26:40', '2019-09-11 05:26:40'),
('d993db4f979cfe97aeb8aea43df02e10a5f20d53d9978c288f535ec6fd19bcbc67e338c93c775ab0', 36, 1, 'MyApp', '[]', 0, '2018-09-18 00:18:06', '2018-09-18 00:18:06', '2019-09-18 07:18:06'),
('dc88801c572dc79e56e0d5e2b8f00dc61f73f4ceabba1e61617d263e9f8e86881b47801abca9c3b6', 21, 1, 'MyApp', '[]', 0, '2018-09-03 01:16:32', '2018-09-03 01:16:32', '2019-09-03 08:16:32'),
('dd2449832751e893a39ca38e4b306799be42f51a15df6b5540be8fa7b3e3e82265afe079cd6c3992', 36, 1, 'MyApp', '[]', 0, '2018-09-03 02:22:03', '2018-09-03 02:22:03', '2019-09-03 09:22:03'),
('dd6114f35afb5cacb322e2e3dac68b719a0c8462320664feb647310e65be8e854c79f8256772baa8', 36, 1, 'MyApp', '[]', 0, '2018-09-04 03:37:32', '2018-09-04 03:37:32', '2019-09-04 10:37:32'),
('dddc6d4f2efc598a3a061ecca8b2d4c5e61b8ac68671a6338ad7225dc5b43288581de22f8329c7b5', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:53:15', '2018-09-12 21:53:15', '2019-09-13 04:53:15'),
('ddf6520b79bedd7530adf2bfd7ecf1185294e705e7344ebfcecc85650ff4ed08e16a69412853673d', 37, 1, 'MyApp', '[]', 0, '2018-09-03 03:03:24', '2018-09-03 03:03:24', '2019-09-03 10:03:24'),
('de034877d8b9974dea9caba94e68ed0e3773b6abefd5929cee7bf90d4b81bf48be6c896729db9864', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:26:31', '2018-09-10 22:26:31', '2019-09-11 05:26:31'),
('e21f1d85bdd2f3f7aa39d509dbef88792495a6491ee293fd34325cddcf3d4065edf86b8d7642ce72', 36, 1, 'MyApp', '[]', 0, '2018-09-18 00:13:43', '2018-09-18 00:13:43', '2019-09-18 07:13:43'),
('e3052e7d4edea261fa0c7e70bac3e44fbde36cc33fc964f03ee2db421efc031e788234ee75b00954', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:54:15', '2018-09-12 21:54:15', '2019-09-13 04:54:15'),
('e57bc65b298e6f4ee0d1120c3d17fca28ab4ec3d4c6c582ffe85fc3a836eadc48e739f0727d48383', 8, 1, 'MyApp', '[]', 0, '2018-09-02 23:42:01', '2018-09-02 23:42:01', '2019-09-03 06:42:01'),
('e64dc6e909fcabef5905f76d8aac7ad959e1a16569e95462a4fcc72732d3d47f45f693eecdd3c843', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:10:24', '2018-09-10 22:10:24', '2019-09-11 05:10:24'),
('eb217e7636455f2b5df12b4cfcfc9e15e94502242f72ea4315c3d292a6f032ad48d8a7f708a5822d', 34, 1, 'MyApp', '[]', 0, '2018-09-12 22:03:04', '2018-09-12 22:03:04', '2019-09-13 05:03:04'),
('ed2570bd556029ee56fc5e5d8123f5ea53ed7455e91dd51b206b9129a04257f66e92fb243e65f29c', 20, 1, 'MyApp', '[]', 0, '2018-09-03 01:15:47', '2018-09-03 01:15:47', '2019-09-03 08:15:47'),
('eee2b715bf3260fc3ae2adecbbf2dac96ea4f51d9786a2952dee0208addf115116ac1978d273a228', 34, 1, 'MyApp', '[]', 0, '2018-09-10 21:44:05', '2018-09-10 21:44:05', '2019-09-11 04:44:05'),
('f0df780aca15ccab1118578185c1c19c04a6167bc32b2f2c89a2f3d27dad45e3072a3707e06d52bd', 37, 1, 'MyApp', '[]', 0, '2018-09-10 22:11:51', '2018-09-10 22:11:51', '2019-09-11 05:11:51'),
('f3306ef49d7e0d04ab88d4e81a99d9551eb85d8c7f7293c6b8dd534b16e6e162cc76c17a573037a7', 34, 1, 'MyApp', '[]', 0, '2018-09-12 21:09:26', '2018-09-12 21:09:26', '2019-09-13 04:09:26'),
('f4e1e48d78c84c06c5b2fff26a8a13eb34c73048690ede64521dc184e8a3c8f8b8ed7c280c0f80c6', 19, 1, 'MyApp', '[]', 0, '2018-09-03 01:15:22', '2018-09-03 01:15:22', '2019-09-03 08:15:22'),
('f67d38b3fb551c4ef018be84df320587f7c81e766b24c65e8f2f60a8ae5a14d4a908078c83dac5d5', 37, 1, 'MyApp', '[]', 0, '2018-09-10 21:56:31', '2018-09-10 21:56:31', '2019-09-11 04:56:31'),
('f731d8661bf0b7729ec69af998b1453716185f19f60e5126a9eeb612d181f8ecc9c143f61fc31d41', 6, 1, 'MyApp', '[]', 0, '2018-09-02 22:14:53', '2018-09-02 22:14:53', '2019-09-03 05:14:53'),
('f7c9b0c951ca2e137a23d81bbed4d7c8b3d4017c239f2bcc11b47bd2c5b7426af165db4acf9c58fb', 34, 1, 'MyApp', '[]', 0, '2018-09-10 20:49:34', '2018-09-10 20:49:34', '2019-09-11 03:49:34'),
('f88e800fe2ffa6e13fffd757bdb85330f17d56d1013d06779199964bd5b08c400b929e5e2850d772', 35, 1, 'MyApp', '[]', 0, '2018-09-03 02:25:23', '2018-09-03 02:25:23', '2019-09-03 09:25:23'),
('fb8feddc83ed875423734c7f0fcd3bc22f643b4bb664b9f55302bc2c06cc4dfe2f13b1dddb2d2a39', 37, 1, 'MyApp', '[]', 0, '2018-09-03 02:31:23', '2018-09-03 02:31:23', '2019-09-03 09:31:23'),
('fe8590560d429087f05a7441ba1e8db9f93a524dfbee1437e547f7c6ee17dbe2d10caed5b2bb1bde', 27, 1, 'MyApp', '[]', 0, '2018-09-03 01:28:06', '2018-09-03 01:28:06', '2019-09-03 08:28:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'UKfKVnwXBmLFtWn4GLqkjShyRSWQUPtxXfwPB410', 'http://localhost', 1, 0, 0, '2018-09-02 21:52:16', '2018-09-02 21:52:16'),
(2, NULL, 'Laravel Password Grant Client', 'smOf1iLpFR7vI4wnfwOrGZKap0CjbGUQGZfhcZ85', 'http://localhost', 0, 1, 0, '2018-09-02 21:52:16', '2018-09-02 21:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-09-02 21:52:16', '2018-09-02 21:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `pos_id` int(5) NOT NULL,
  `pos_name` varchar(30) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`pos_id`, `pos_name`) VALUES
(1, 'พนักงานต้อนรับ'),
(2, 'พนักงานเสิร์ฟ'),
(3, 'พนักงานครัว');

-- --------------------------------------------------------

--
-- Table structure for table `settask`
--

CREATE TABLE `settask` (
  `set_id` int(11) NOT NULL COMMENT 'ไอดี แทค',
  `set_month` int(1) NOT NULL COMMENT 'พนักงานประจำหยุดได้กี่วัน/เดือน',
  `set_task` int(1) NOT NULL COMMENT 'พนักงานชั่วคราวหยุดได้กี่วัน/แทค',
  `set_alter` int(1) NOT NULL COMMENT 'แก้ไขวันทำงานได้กี่ครั้ง',
  `set_delegate` int(1) NOT NULL COMMENT 'แทนงานได้กี่วัน '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `star`
--

CREATE TABLE `star` (
  `star_id` int(11) NOT NULL COMMENT 'รหัสดาว',
  `star_1` int(1) DEFAULT NULL COMMENT ' รูปร่างหน้าตา 1',
  `star_2` int(1) DEFAULT NULL COMMENT ' รูปร่างหน้าตา 2',
  `star_3` int(1) DEFAULT NULL COMMENT 'การทำงาน 1',
  `star_4` int(1) DEFAULT NULL COMMENT 'การทำงาน 2',
  `star_5` int(1) DEFAULT NULL COMMENT 'การปฏิสัมพันธ์กับลูกค้า ',
  `emp_id` int(5) NOT NULL COMMENT 'รหัสพนักงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `star`
--

INSERT INTO `star` (`star_id`, `star_1`, `star_2`, `star_3`, `star_4`, `star_5`, `emp_id`) VALUES
(26, 3, 1, 2, 3, 5, 1),
(27, 1, 3, 5, 4, 3, 2),
(28, NULL, NULL, NULL, NULL, NULL, 3),
(29, NULL, NULL, NULL, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `strike`
--

CREATE TABLE `strike` (
  `strike_id` int(11) NOT NULL COMMENT 'รหัสหยุดงาน',
  `strike_date` date NOT NULL COMMENT 'วันที่จะหยุดงาน',
  `strike_status` tinyint(1) NOT NULL COMMENT 'สถะการอนุมัติหยุดงาน 0 ไม่อนุมัติ, 1 อนุมัติ',
  `emp_id` int(5) NOT NULL COMMENT 'id พนักงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL COMMENT 'รหัสแทค',
  `task_name` varchar(255) COLLATE utf8_croatian_ci NOT NULL COMMENT 'ชื่อแทค',
  `task_start` date NOT NULL COMMENT 'วันเริ่มแทค',
  `task_end` date NOT NULL COMMENT 'วันสิ้นสุดแทค',
  `task_status` tinyint(1) NOT NULL COMMENT 'สถานะการเปิดใช้งาน 0 ปิด, 1 เปิด'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` varchar(10) COLLATE utf8_croatian_ci NOT NULL COMMENT 'id ผู้ใช้งาน',
  `user_pass` varchar(20) COLLATE utf8_croatian_ci DEFAULT NULL COMMENT 'รหัสผ่าน ผู้ใช้งาน',
  `user_status` int(1) NOT NULL COMMENT 'สถานะ 1 Employee, 2 Supervisor, 3 Manager, 4 Admin',
  `emp_id` int(5) NOT NULL COMMENT 'id พนักงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_pass`, `user_status`, `emp_id`) VALUES
('0811111111', NULL, 1, 1),
('0822222222', NULL, 2, 2),
('0833333333', NULL, 3, 3),
('0844444444', NULL, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(34, '0811111111', NULL, '$2y$10$MzQt8dZJC6XhR0TzBCZaduxVOPvnxyvSQwVd.B5kgXMvdEVivfKKa', NULL, '2018-09-03 02:21:33', '2018-09-12 02:33:35'),
(35, '0822222222', NULL, '$2y$10$Ag6QNtq2ZiDWEoiUhh7k2Olba9MhuH.P48AqcrQTk.3ioXp5VAChe', NULL, '2018-09-03 02:21:47', '2018-09-12 02:33:44'),
(36, '0833333333', 'john@foo.com', '$2y$10$Kf2KH/bAZuN4MdyMLx1loewHFIInc5yMfnffP2fwykteO01q5Odli', NULL, '2018-09-03 02:22:02', '2018-09-12 02:33:52'),
(37, '0844444444', NULL, '$2y$10$e3HgICOXgge7z7ixvKv7Luko67bvD0SqDyipFvNIDNINCum1dtY9u', NULL, '2018-09-03 02:22:59', '2018-09-12 02:34:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `delegate`
--
ALTER TABLE `delegate`
  ADD PRIMARY KEY (`delegate_id`),
  ADD KEY `user_id` (`emp_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `emp_position` (`emp_position`);

--
-- Indexes for table `fingetscan`
--
ALTER TABLE `fingetscan`
  ADD PRIMARY KEY (`scan_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notstrike`
--
ALTER TABLE `notstrike`
  ADD PRIMARY KEY (`notStrike_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `settask`
--
ALTER TABLE `settask`
  ADD PRIMARY KEY (`set_id`);

--
-- Indexes for table `star`
--
ALTER TABLE `star`
  ADD PRIMARY KEY (`star_id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `strike`
--
ALTER TABLE `strike`
  ADD KEY `user_id` (`emp_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delegate`
--
ALTER TABLE `delegate`
  MODIFY `delegate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแทนงาน';

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'id พนักงาน', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fingetscan`
--
ALTER TABLE `fingetscan`
  MODIFY `scan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแสกนลายนิ้วมือ';

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `history_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'รหัสประวัติการใช้งาน';

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notstrike`
--
ALTER TABLE `notstrike`
  MODIFY `notStrike_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดีห้ามหยุด';

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `pos_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settask`
--
ALTER TABLE `settask`
  MODIFY `set_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดี แทค';

--
-- AUTO_INCREMENT for table `star`
--
ALTER TABLE `star`
  MODIFY `star_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสดาว', AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแทค';

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `delegate`
--
ALTER TABLE `delegate`
  ADD CONSTRAINT `delegate_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`emp_position`) REFERENCES `position` (`pos_id`);

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `star`
--
ALTER TABLE `star`
  ADD CONSTRAINT `star_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `strike`
--
ALTER TABLE `strike`
  ADD CONSTRAINT `strike_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
